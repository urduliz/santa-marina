# Grupo de trabajo (lan-talde)

```{toctree}
:hidden:

parcelas
senalizacion
jardineria
escalones
```

En principio, un grupo de trabajo (lan-talde) se encargará de discutir y elaborar propuestas concretas que se remitan
a otros grupos de trabajo del pueblo para obtener comentarios y resolver posibles conflictos; para después remitirlas al
equipo de gobierno para su tratamiento en el mismo y en el pleno municipal.

El 23, 24 o 30 de septiembre, o el 1 de octubre, quedaremos media docena de vecinos interesados para pasear por el cordal.
La idea es hacer un recorrido similar al del 9 de septiembre, pero recogiendo sensibilidades de un abanico más amplio.
Idealmente algunos o todos los participantes en esa quedada estarán potencialmente dispuestos a participar en el grupo
de trabajo.

La dinámica del mismo (frecuencia de reuniones, comunicación asíncrona/síncrona, etc.) y las herramientas de
comunicación a utilizar están por definir aún.

## Actuaciones/tareas concretas

### ¿Sugerencia de no escalar si ha llovido?

En el Eskalada Eguna del 9 de septiembre de 2023, Nagore comentó con mucho acierto que quizás deberíamos coger la
costumbre de no escalar en Urduliz si ha llovido el día anterior o dos días antes.
Que nos aseguremos de que la roca está seca, porque sino se desgasta y parte más fácil.

Aunque los sectores "tradicionales" están muy usados y ya han saltado casi todas las regletas o aristas que estuvieran
débiles, en los sectores más recientes es un factor a tener en cuenta.
