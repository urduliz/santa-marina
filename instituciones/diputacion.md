# Diputación

Desde 2021 parece haber subvenciones específicas para "instalationes deportivas supramunicipales", entre las que se
entiende que se incluyen las escuelas de escalada.
Hubo una convocatoria para 2021 y 2022, y en abril de 2023 se anunción la apertura del plazo para 2023 y 2024.

- [ORDEN FORAL 3312/2021, de 14 de diciembre, de la diputada foral de Euskera, Cultura y Deporte, por la que se resuelven las solicitudes de subvención presentadas al amparo del Decreto Foral 115/2021, de 24 de agosto, de la Diputación Foral de Bizkaia, por el que se aprueban las bases reguladoras y la convocatoria de subvenciones forales destinadas a entidades locales del Territorio Histórico de Bizkaia para la realización de inversiones en instalaciones deportivas supramunicipales durante los años 2021 y 2022.](https://www.bizkaia.eus/lehendakaritza/Bao_bob/2021/12/16/I-1044_cas.pdf)
  - [17 de abril de 2023 11:30; Abierto el plazo para solicitar las ayudas destinadas a la realización mejoras en instalaciones deportivas supramunicipales](https://www.bizkaia.eus/es/web/comunicacion/noticias/-/news/detailView/25379)

Sería muy recomendable que el Ayuntamiento de Urduliz consultara cómo acceder a dicha subvención, y qué actuaciones
están al amparo de la misma.

(medio-ambiente)=
## Medio Ambiente y Guarda

Aunque no he podido obtener información/documentación concreta, parece que ha habido ciertos conflictos con Medio
Ambiente y con el Guarda forestal de la zona.

Por un lado, uno de los carteles del Itinerario de la Memoria (el del Mirador de Lukiatxa) se puso siguiendo el principio
de que en ocasiones es mejor pedir perdón que pedir permiso.
Nadie lo ha quitado, y su estado es bastante bueno a pesar de estar tan expuesto a las inclemencias del tiempo.

Por otro lado, Luis Miguel Eguiluz dice [en su blog](https://luismigueleguiluz.blogspot.com/p/inicio.html) que la
Diputación lo denunció y que llegó a un acuerdo con los guardas por lo que si se toca *"cualquier sendero o lo que sea
podrían incluso cerrar la escuela"*.
Sin duda, esto resulta contradictorio con el hecho de que no se haya desecho nada de lo que se equipó en 2018-2019, que
otro departamento/sección de la Diputación anuncie la reequipación en el 2022, y que siga habiendo equipadores creando
vías sin que hayan tenido incidentes con las autoridades.
¿Es posible que el pedir perdón en vez de permiso aplique aquí tambíen y que la preocupación de las autoridades con Luismi fuera la publicidad? Si realmente estuvieran revisando y cuidando periódicamente el entorno, habrían observado los cambios que yo he visto en las últimas semanas sólo paseando y preguntando.

El problema parece ser un narciso que podría estar protegido.
Es suficientemente importante como para informarnos debidamente y conocer en qué zonas se encuentra.
Pero resulta a todas luces ridículo plantear el cierre del cordal porque de facto no se ha realizado ningún control
activo en décadas.

Gracias a AitorS, tenemos las siguientes referencias:
- [Narcissus triandrus L. en el Herbario digital Xavier de Arizaga](http://herbario.ian-ani.org/pliego.php?c=pliegos&a=ficha&i=877)
- [Narcissus triandrus L. subsp. triandrus en el Sistema de Información de la Naturaleza de Euskadi](https://www.ingurumena.ejgv.euskadi.eus/ac84aBuscadorWar/especies/12558)

De acuerdo con las descripciones, el espécimen sólo está localizado en las peñas de Urduliz y en la zona de Maeztu en Alava.
Sin embargo, el mapa muestra una observación en Plentzia también.
Por lo tanto, aunque puede tratarse de una especie protegida, no es endémica de las peñas, y no es la única localización
en Bizkaia.

Nótese la flasedad o imprecisión en la [observación realizada en 2018](https://www.ingurumena.ejgv.euskadi.eus/ac84aBuscadorWar/observaciones/3338666):

![](../_static/img/NarcisusObservacion.png)

La *Localidad* se describe así:

> Santa Maria, Ermitaren hego-ekialdean. Justu Urduliz eta Sopela arteko muga.

Y los *Comentarios* dicen:

> Amadorrek fruitu asko identifikatutako eremua, eskalada eskola berri bat ireki dute, gure bisita baina egun batzuk
> lehengao.
> Eskola honetara iristeko bidea desbrozatu dute, tartean Narcissus triandrus eta Ruscus aculeatusak desbrozatuz.

La escuela de escalada existe desde al menos 30 años antes de que se registrara el espécimen en 2018.
Las vías que se abrieron en 2018 y 2019 no modificaron sustancialmente la peña de Santa Marina, que ya se encontraba
equipada desde los años 80 y se usaba para la práctica de la escalada desde al menos los 70.
Más aún, el acceso por el sureste de Santa Marina es el camino tradicional para acceder a la puerta trasera de la Ermita
o para acceder a la cima (donde están la cruz y la ikurriña).
Ese camino ha sido desbrozado tradicionalmente por vecinos.
Ahondando más, el Itinerario de la Memoria utiliza ese mismo sendero para acceder al nido de ametralladora que hay detrás
de la Iglesia.
Ahondando aún más, en el sureste de la Ermita, en el punto más sombrío (por ser el noreste de la pared), hay una placa y
flores en memoria de alguien.
Por lo tanto, parece tendenciosa la mención de la escuela de escalada dada su irrelevancia con respecto a otras
actividades (incluyendo el desbrozado/poda por parte del ayuntamiento o la mancomunidad) en lo que respecta a la
protección del espécimen.

Alternativamente, lo que es incorrecto podría ser la localización, puede que por no ser de la zona.
Si se refiriera al sureste de Moruatxa y su Arista, sí podría considerarse que la reequipación se realizó en 2018, ya
que esas vías se usaron en los 70, pero no tuvieron tanta actividades durante los 80 y 90 como sí la tuvo Santa Marina.
Pero las coordenadas GPS parecen indicar claramente la subida tradicional a Santa Marina.

Por lo tanto, la existencia del narcisus parece ser irrelevante para la práctica de la escalada en todo el cordal.
Ni siquiera está en un sitio de paso o aseguramiento para escaladores.
Pero si se quiere proteger, sin duda es necesario acordonarlo para evitar que vecinos, paseantes y turistas (que
ascienden por ahí) lo pisen.

Asimismo, deben tenerse en cuenta las otras dos observaciones realizadas en el municipio, en la calle Goieta de arriba y
en la de abajo, ya que esas se encuentran muy cerca de carreteras.

```{important}
El 7 y 8 de septiembre intenté infructuosamente ponerme en contacto con 945019542, que es el teléfono de contacto de
Adolfo Uriarte Villalba, Director de Patrimonio Natural y Cambio Climático, suponiendo que es el responsable del Sistema
de Información de la Naturaleza de Euskadi.
El 13 de septiembre volví a intentarlo, y me respondió una mujer.
Le expliqué que quería:
- Sugerir la corrección de los comentarios en las observaciones del narciso.
- Conocer las medidas de protección a aplicar en caso de realizar actuaciones en la zona.

Me indicó que debo dirigir las consultas por registro al Servicio de Patrimonio Natural, que lo recepcionará y atenderá
su compañera Nerea.
```

### Nidificación

Aunque no es un conflicto en todo el cordal, hay puntos concretos de nidificación.

En [montanaregulada.org/area/urduliz](https://montanaregulada.org/area/urduliz), está indicada la prohibición en tres
vías de Santa Marina N (Kaioa, Salamandra, Lasaiki) por nidificación de cernícalos del 1 de marzo al 30 de junio.

```{important}
Comentándolo con escaladores, nos han indicado que la restricción de tres vías les parece insuficiente, ya que en
Urduliz las vías están muy cerca.
Sugieren que la prohibición para proteger ese punto de nidificación debería extenderse al menos dos vías más a cada lado.
```

Hay una asociación en Sopela que surgió en verano de 2021 llamada Kiribile
([Asociación KIRIBILE Sopelako Eraztun Berdearen aldeko Elkartea](https://www.euskadi.eus/elkartea/asociacion-kiribile-sopelako-eraztun-berdearen-aldeko-elkartea/web01-ejeduki/eu/)),
cuyo objetivo es promover la conservación y mejora del patrimonio natural e histórico-cultural de Sopela.
Según la prensa (ver [deia.eus/bizkaia/2022/04/25/vecinos-sopela-apuestan-disenar-anillo-1708053.html](https://www.deia.eus/bizkaia/2022/04/25/vecinos-sopela-apuestan-disenar-anillo-1708053.html)),
está compuesta por una quincena de vecinos y vecinas, y el único nombre propio que se menciona es el de Aitor Zubiaga.
Parece ser nuestra mejor fuente de información, aunque bastante *analógica*.
Itziar (la alcaldesa de Urduliz) está en contacto con ellos de forma indirecta.
Alguno de los vecinos que está limpiando el sendero también se ha puesto en contacto con ellos, a través de un tercero,
Gorka, escalador y conocido de uno de los integrantes de Kiribile.

Les han hecho saber que hay nidos de lechuza entre Santa Marina y Ander Deuna.
Sería interesante especificar si es entre Santa Marina y la curva de Torre Barri, o al oeste de la curva.

```{important}
Es en general muy positivo que haya varios canales de comunicación con Kiribile.
Sin embargo, la comunicación a través de varias bocas puede resultar lenta e imprecisa.

Resulta necesario conocer una fuente más adecuada para informarnos de este tipo de prohibiciones/limitaciones.
¿Sabemos si Kiribile tiene alguna sección en la web del Ayuntamiento de Sopela, de la Diputación u otro medio?
¿Elaboran algún tipo de informe que podamos descargar/transmitir con un registro/formato medianamente formal?

Lo más similar que he podido encontrar es lo siguiente:

- Mapa del anillo verde de Sopela en la siguiente noticia: [deia.eus/bizkaia/2023/01/17/sopela-presenta-mapa-itinerario-anillo-6350186.html](https://www.deia.eus/bizkaia/2023/01/17/sopela-presenta-mapa-itinerario-anillo-6350186.html)
- Sección/página en la web del Ayuntamiento de Sopela: [sopela.eus/areas-municipales/sostenibilidad/anillo-verde-de-sopela](https://sopela.eus/areas-municipales/sostenibilidad/anillo-verde-de-sopela/)
  - Plano en PDF (más simple que el de la noticia en prensa, y sin leyenda): [sopela.eus/sostenibilidad/2023_eraztun_berdearen_planoa.pdf](https://sopela.eus/sostenibilidad/2023_eraztun_berdearen_planoa.pdf)
  - Acuerdo estratégico firmado en Abril de 2022 por diferentes partidos políticos y asociaciones: [sopela.eus/sostenibilidad/2022_eraztun_berdea_akordio_estrategioa.pdf)](https://sopela.eus/sostenibilidad/2022_eraztun_berdea_akordio_estrategioa.pdf) (nótese que la firmante en representación de Kiribile es Sonia Polo, en
  vez de Aitor Zubiaga mencionado en la noticia de Abril de 2022).
```

```{important}
Asimismo, sería recomendable indicar estas limitaciones en la propia pared.
Ahora mismo, no hay ninguna indicación concreta en el sitio, ni en la pared ni en el cartel de la Diputación;
aunque a veces hay cintas puestas en las primeras chapas/químicos de las vías a evitar.
```
