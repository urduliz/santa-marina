# Fuentes/manantiales

Aunque este repositorio está centrado en las Peñas de Santa Marina, y la cuenca del Gobelas en Urduliz, sería
interesante recopilar también la ubicación de otros manantiales/fuentes en el pueblo (los que desgüan al Butrón), con el
propósito de elaborar itinerarios/paseos que las recorran.

Como, por ejemplo, la fuente de Ganbelarre que me mostró Ugo.
