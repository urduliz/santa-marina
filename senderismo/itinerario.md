(itinerario)=
# Itinerario de la Memoria

- El paso del Parque de la Memoria a la calle Goieta no está indicado. El itinerario recomendado y dibujado en todos los carteles es bajar de Santa Marina a la Iglesia por la carretera y recorrer la calle Goieta desde el principio a la altura del Hospital. Resulta mucho más natural empezar en Santa Marina, recorrer el paseo hacia el Parque de la Memoria y al final de este continuar por Goieta directamente. El paso son 4-5 m de maleza/zarzas que los vecinos conocen y utilizan habitualmente; por lo que sólo requeriría desbrozar ligeramente y añadir un cartel/poste.
  - En alguno de los documentos de la web del Ayuntamiento se muestra que el plan era que hubiera una pequeña pista en la linde con la Hípica. Es posible que por cuestiones presupuestarias finalmente no se hiciera esa pista, por no hacer el acceso de la misma a la calle Goieta o por no poder adquirir los terrenos (ver sección SigPac).
- Es posible acceder caminando desde Pino Alto hasta Erdikoatxa por la cresta, y de ahí al Mirador de Lukiatxa, donde está el cartel del Mirador en el Itinerario de la Memoria. Es una opción interesante porque permite hacer un recorrido circular, descendiendo después por la pista forestal que es el Itinerario recomendado. Además, es una dirección de recorrido interesante, ya que los puntos de interés van de menos a más, encontrando primero trincheras, después un nido, y al final el nido doble junto al nido triple. Resulta por lo tanto llamativo que el Itinerario indicado en los carteles ignore esta opción y recomiende subir y bajar prácticamente por el mismo camino, incrementando el tráfico de paseantes en la calle donde hay vehículos.
  - Tengo la sensación de que esto puede deberse a que una parcela de unos 50 m (donde se encuentra Erdikoatxa) es privada, por lo que la Diputación la evitó al realizar el Itinerario. Sin embargo, está notablemente pisado, los carteles que pudieran indicarlo tienen sólo el poste (están rotos), y las vallas están rotas en la linde con la cresta.
- Los siguientes tracks, realizados por dos usuarios particulares en 2020 y 2022, muestran estas dos alternativas:
  - https://es.wikiloc.com/rutas-senderismo/penas-de-santa-marina-en-urduliz-93942084
  - https://es.wikiloc.com/rutas-senderismo/santa-marina-moruatxa-erdikoatxa-lukiatxa-fraidemendi-49947692
  - Nótese que ambos usuarios intentaron acceder de Muruatxa N Alto hacia el E, y ambos se dieron la vuelta tras encontrar los dos rápeles indicados anteriormente.

## Acceso del Parque de la Memoria a la calle Goieta

## Haurren basoa

## Nido en propiedad particular

## Nido destruido en Hípica

## Peligro derrumbamiento abrigos/refugios

## Enlace con Munarrikolanda

