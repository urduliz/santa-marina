# Canteras, cuevas y Marinas

Además de recorridos/paseos por lugares de interés tangibles (el cordal, monumentos del Cinturón de Hierro,
fuentes/arroyos/canales, etc.) también podemos visitar lo que no es tan visible por la intensa actividad que las diezmó
el siglo pasado.

::::{card-carousel} 3

:::{card}
(SantaMarina1899)=
```{figure} ../_static/img/koldo/SantaMarina1899.jpeg

Santa Marina, 1899, revista 'Euskalzale'.
Extraída del artículo de Koldo Somocueto en Urduliz Gaur 29.
```
:::

:::{card}
```{figure} ../_static/img/koldo/santa_marina_1954_01.jpg

Fotografía realizada por Indalecio Ojanguren en 1954,
procedente de guregipuzkoa.eus (CC BY-SA),
facilitada por Koldo Somocueto.
```
:::

:::{card}
```{figure} ../_static/img/koldo/santa_marina_1954_02.jpg

Fotografía realizada por Indalecio Ojanguren en 1954,
procedente de guregipuzkoa.eus (CC BY-SA),
facilitada por Koldo Somocueto.
```
:::

:::{card}
(SantaMarina1955)=
```{figure} ../_static/img/koldo/santamarine_1955.jpg

Cordal de Santa Marina, 1955.
Fotografía cedida por Koldo Somocueto.
```
:::

::::

## Canteras

Hay varias antiguas canteras en el cordal.
Puede que hubiera hasta una decena o docena, entre Urduliz y Sopela.
Algunas de ellas son todavía muy evidentes (Atxarte, Erdikoatxa y Santa Marina), pero otras no tanto.
Por ejemplo, {numref}`SantaMarina1955` muestra una peña al Oeste de Santa Marina que ya no está.

Sería interesante localizar/situar todas las canteras que hubo y trazar un recorrido circular que permita visitar las
bases, pasillos o paredes que queden.

## Cueva

> NOTAS ARQUEOLÓGICAS. KOBIE (Bilbao) Grupo EspeleolÓgico Vizcaíno, Boletín nº4. Excma, Diputación de Vizcaya.
>
> 70º- Cueva de Santa Marina: En una peña de Urduliz se abre la cueva donde esta la ermita de Santa Marina. (21)

De acuerdo con Koldo Somocueto, fue una hipótesis que lanzó Barandiaran, de forma muy acertada, sobre el origen de la
ermita, dada la morfología del lugar en el que se encuentra.

```{note}
Existen muchos casos de cristianización de lugares de rito preexistentes.

Requieren mención especial la cabecera del río Vero en la Sierra de Guara y la Ermita de San Martín de Lecina.
La lectura de los artículos [El camino de las Escaleretas](http://periploabq.blogspot.com/2016/06/el-camino-de-las-escaleretas-acerca-de.html),
de Anabel Moreno Laguarda y Enrique Salamero Pelay, y [Santuario solsticial del Vero](http://a0avista.blogspot.com/2020/01/santuario-solsticial-en-el-vero-san.html) de [Álex](https://www.blogger.com/profile/10997870659691303172), en Julio de
2023, inspiraron el nacimiento de esta página que visitas.
``````

Sin embargo, como elabora Koldo en Urduliz Gaur 21, pudo haber otra cueva que bien fue destruía por las canteras o
espera ser redescubierta.

## Las Santas Marinas

> https://core.ac.uk/reader/11497680
>
> existe como leyenda, en Vizcaya, que las Santa Marinas eran tres hermanas vírgenes y que las tres se veían desde sus
> respectivas moradas: la una está en el monte Arrola, paraje de Guesuri (Orozco), la otra estaba en Ganguren-gane
> (Galdacano) y la otra se halla en las peñas de Urduliz.

> DOLMENES Y SARCOFAGOS MISTERIOSOS, Juan GOROSTIAGA
>
> También en Vizcaya existe algo análogo en las Visitas que Santa Marina del elevado peñascal de Urdúliz hace a Santa
> Marina de Ganguren *28 «el somo».
>
> 28: Oído por mí mismo en las cercanías del lugar.

¿Podemos encontrar algo más sobre esta leyenda?
¿Es realmente posible ver esas otras dos cimas (en Orozko y Galdakao) desde lo alto de Santa Marina?

En [ermitasdevizcaya.com](http://ermitasdevizcaya.com/) encontramos algo más de información sobre esas dos ermitas
hermanas.
Parece ser que la de Galdakao fue demolida en 1782, pero se colocó una cruz de piedra en su lugar.
La de Orozko sigue en buen estado de conservación:

- [ermitasdevizcaya.com/ERMITAS_GALDAKANO.pdf](http://ermitasdevizcaya.com/ERMITAS_GALDAKANO.pdf)
- [ermitasdevizcaya.com/OROZK_STAMDG_GA1.jpg](http://ermitasdevizcaya.com/OROZK_STAMDG_GA1.jpg)
  - [ermitasdevizcaya.com/OROZK_STAMDG.htm](http://ermitasdevizcaya.com/OROZK_STAMDG.htm)

## Pinturas

A primeros de agosto me encontré con tres chavales de 12-14 años en Lukiatxa.
De hecho, era la primera vez que iba y me indicaron el camino.
Estaban subiendo con las bicis a cuestas para después bajar por la cuesta.

Paseando por las trincheras y abrigos, me dijeron que rodeando por el norte alguno de los bloques encontraron una
pintura de un caballo/bisonte y que avisaron al Ayuntamiento.
Rodeamos uno de los bloques, avanzando muy penosamente, y no encontramos nada a primera vista.

Sería interesante preguntar si alguien del Ayuntamiento recuerda haber recibido aquel aviso.
Al mismo tiempo, pasear de nuevo con más detenimiento.

## Molinos

En varias páginas se hace referencia a que existieron ocho molinos en Santa Marina:

> https://www.urduliz.eus/es-ES/turismo/Historia/Paginas/default.aspx
>
> La ermita de Santa Marina está situada en las peñas del mismo nombre, donde existieron ocho molinos.
>
> A fines del siglo XIX se construyó el castillo de Butrón con material extraído de las canteras de Urduliz.​​

> https://www.bizkaia.eus/documents/7171139/12291263/62+-+URDULIZ+AYTO+ISAD.pdf
>
> Contaba con ferrerías y molinos. También ha contado con canteras.

Sin embargo, no parece haber restos arqueológicos.

Según Koldo Somocueto, se trata de un error que se ha repetido una y otra vez.
Hubo ocho molinos, pero de agua, en los diferentes arroyos, concretamente dos de ellos, que tiene Urduliz.
Lo de los ocho molinos de viento es poco más que ficticio.
Sí se construyeron molinos de viento en Bizkaia en el siglo XVIII durante una sequía, pero fueron menos de media docena
en todo Bizkaia y tuvieron una vida muy corta, por lo que no parece razonable que hubiera ocho en un solo cordal.
Por lo que muy probablemente se trata de un equívoco repetido.
