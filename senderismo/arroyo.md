(urdunlaitz)=
# Aguas al norte de Santa Marina

Los caminos del agua de Santamariñe a GOiBELAtS en URDU(n)LaItZ.

```{important}
La elaboración de esta sección habría sido imposible sin la información de más de una docena de vecinos y vecinas.
Bihotz-bihotzez, eskerrik asko.
Ante un tío que se presenta en su casa a cualquier hora de la mañana o de la tarde y se presenta como "un vecino que
quiere saber sobre las aguas que vienen de las peñas", todos han respondido con sorprendente amabilidad y disposición a
compartir lo que saben/recuerdan.
Parece ser que este tema nos toca la patata por igual a varias generaciones y está siendo muy gratificante la mezcolanza
de nostalgia de quienes vivieron esos arroyos y manantiales con la sed de aventura/descubrimiento de las generaciones
posteriores.

No obstante, nótese que quien escribe no es periodista ni historiador, por lo que no he grabado ninguna conversación ni
tengo un registro concreto de testimonios.
De hecho, tengo buena memoria visual, pero soy muy malo para los nombres; por lo que recuerdo en qué casas he estado y
con quién he hablado, pero no sería capaz de reproducir los nombres y apellidos.
Si alguien desea dotar de cierto formalismo al contenido aquí expuesto, por favor no dudes en ponerte en contacto.
```

## Contexto

Es *vox populi* en el pueblo que *"el Gobela(s) está soterrado a su paso por Urduliz"*, pero las generaciones posteriores
a los setenta no sabemos hasta qué punto es una leyenda o tiene algo de cierto que *"el nacimiento del río Gobela(s) está
en las peñas"*.

Anecdóticamente, la habitación en la que más tiempo he dormido en mi vida está en Marusas/Mauratza, justo encima del
arroyo canalizado.
Tengo recuerdos de muchas noches quedándome dormido imaginando cómo sería ese arroyo/río.
Lento y silencioso en verano, ensordecedoramente ruidoso y turbulento con las lluvias de otoño, y heladoramente frío en
invierno.
Nunca lo vi, ya que estaba ya soterrado cuando empecé a tener cierta consciencia.
Mas no deja de sorprenderme la presencia que ha tenido y tiene en mi imaginario.
Supongo que como las estrellas y el fuego, no podemos evitar la fascinación por el agua.
Si las tormentas hacían correr el agua así por las calles, qué habría debajo.
En mi cabeza, el arroyo venía de un sitio muy muy lejano, mucho más que Mungia, y acaba en un sitio más lejano aún, más
allá de Sopela(na).
Por eso, cuando hace unas semanas un vecino, Ander, me dijo que andaban buscando el nacimiento, mis recuerdos se
tambalearon.
El río de mis sueños traía aguas muy lejanas, ¿cómo que surgen de la nada bajo las peñas?
Me enamora que ese comentario aparentemente inocente me haya llevado a recorrer el viaje que en estas líneas transcribo.

Aunque encontrar la respuesta concreta pudiera tener algo de pequeño pique entre Sopela, Barrika y Urduliz, lo cierto es
que no resulta fácil entender cuáles son los afluentes "naturales" del río Gobela(s) (ni siquiera si el nombre es Gobela
o Gobelas), y en cualquier caso a dónde van a parar las aguas de todo el norte del cordal.
Vamos a tratar de aclararlo, como excusa para dedicar unos días a pasear virtual y presencialmente por el pueblo, hablar
con vecinos y vecinas, y viajar por nuestra historia reciente antes de que el tiempo nos impida recuperar los recuerdos
por vía oral.

Asumimos que el extremo Este del cordal desagua hacia Zalbiderreka, que es afluente del río Butrón.
Al Butrón desagua también la mayoría del municipio (de Goieta al Norte y al Este).
Por otro lado, desde Frailemendi hasta el camino de Torre Barri o hasta Santa Marina es razonable que filtre
directamente al río Gobela en la calle/carretera Olabide de Sopela y la nueva calle Arteta de Urduliz.
¿Qué sucede con las aguas de la parte central del cordal?

Las aguas al Norte de al menos Santa Marina, Moruatxa y Erdikoatxa en los 90-2000 produjeron pequeñas riadas/inundaciones
en el barrio Marusas/Mauratza de Urduliz, donde ahora se encuentra el Hospital.
Siendo de ese barrio, recuerdo haber estado de madrugada sacando vehículos y poniendo tablones para evitar la inundación
de los trasteros.
Hace años se realizó una obra para reducir la altura de la carretera en la arqueta del centro del barrio, y se redujo
también la altura de la acera en el paso a la calle del Arbelaitz, de forma que dejó de haber problemas:

![](../_static/img/umarcor/CalleArbelaitz.jpg)

Sabemos que ese tramo está soterrado y ha habido varias obras en los últimos 40 años para ir mejorando y agrandando el
canal subterráneo.
¿De dónde venían/vienen y a dónde iban/van a parar esas aguas?

```{note}
Para los impacientes, la respuesta parece ser que el Gobela(s) nace tanto en Barrika como en Urduliz y toma su nombre
cuando sendos arroyos provenientes de dichos municipios (el de Barrika en superficie y el de Urduliz soterrado hace
décadas) se encuentran cerca del triple límite municipal con Sopela, cerca del Garbigune.
Asimismo hay otros dos arroyos que surgen en los mismos cordales (Gane/Gana y Santa Marina), que recorren principalmente
el municipio de Sopela y se encuentran ambos con el Gobela(s) en Urko.

Por lo tanto, es difícil definir un único nacimiento, ya que por cota podrían ser Barrika o Urduliz (Lukiatxa es pocos
metros más alto que Gana, pero habría que ver la cota de las surgencias), por longitud/extensión serían también Barrika
o Urduliz, y por aportación de caudal (área de la cuenca) sería Sopela.
Podemos decir que el Gobela(s) tiene entre dos y tres nacimientos principales y dos secundarios (por unirse
al cauce principal en cotas inferiores).
- En Barrika se encuentran como principal *Agirreko urak* y como secundario *Lemotza*.
  Parece haber dos arroyos a ambos lados de Arriaga, por lo que podría haber más de un nacimiento de *Agirreko urak*.
- En Urduliz se encuentra como principal *Marusas* (no sabemos el nombre), que parece dividirse en
  su parte más alta en hasta tres arroyos; y la fuente Larragoiti (actualmente perdida entre la maleza).
- En Sopela se encuentra como secundario el *Zaituerreka/Muñarrikolanda*, y el *Lemotza* atraviesa el municipio de
  noreste a suroeste para unirse al *Gobela(s)* (que también atraviesa el pueblo de este a suroeste) en el extremo
  suroeste del municipio.

En este sentido, es cierto que al menos un nacimiento del Gobela(s) se encuentra en Urduliz.
Para saber los detalles, ármate de paciencia y sigue leyendo ;).
```

## Toponimia y etimología

Como acertadamente me comentaron varias vecinas: *"cuando éramos pequeños estábamos todo el verano en esta fuente o en
aquel arroyo cogiendo zapaburus; al fin y al cabo, Urduliz viene de ur, agua"*.
Veremos a lo largo de esta sección, que no les falta ni un ápice de razón, tanto etimológicamente como por sus
características geológicas.

Etimológicamente, de acuerdo con [Toponimia en Euskara por Nestor de Goikoetxea y Araluze "Urdiola"](https://www.bizkaia.eus/fitxategiak/04/ondarea/Kobie/PDF/5/Kobie_1_Etnografia_%C2%ABTOPONIMIA%20EUSKARA%C2%BB%20%20por%20Nestor%20de%20Goikoetxea%20y%20Ar.pdf?hash=b22e906560baf320f7349b3240a7db61):

> AITZ y sus variantes AIZ. AZ. ATX. AX, AS, EZ, ITZ e IZ, tienen la significación de peña. Aunque peña y piedra parecen
> palabras sinónimas, no lo son, puesto que peña indica piedra sin labrar, según produce la naturaleza, y piedra es el
> conjunto compacto de la masa rocosa que sirve, cortado en trozos regulares, para la fabricación de paredes, edificios,
> adoquinación de calles y otros varios usos.

En algunos sitios el topónimo es Urdulitz, aunque oficialmente tanto en castellano como en euskera es Urduliz (debiendo
tildar la segunda 'u' siguiendo las normas de acentuación del castellano).

ura dun (duen) aitz(a) > urdunaitz > urdulaitz > urdulitz > urduliz

¿Nos está diciendo el topónimo que Urduliz/Urdulitz son *"las peñas del agua"* o *"las peñas que tienen agua"*?
¿Desde cuándo se sabe que en efecto las peñas acumulan agua en su interior como un inmenso botijo porque hay capas no
permeables?
¿Lo sabían nuestros anteriores y nos lo dejaron claramente (d)escrito en el nombre del pueblo pero se nos ha olvidado?

Veamos si con toda la tecnología y capacidad de comunicación moderna somos capaces de resolver lo que la interpretación
quasiliteral del topónimo nos dice.

Por otro lado, en lo que respecta a la denominación del Gobela(s) parece no haber respuesta unánime.
En [UN APEADERO LLAMADO GOBELA](https://getxosarri.blogspot.com/2018/07/un-apeadero-llamado-gobela.html), un vecino de
Getxo cita varias referencias para sostener que la denominación original es Gobela, probablemente por un caserío, y que
Gobelas es una deformación al ser habitual en la zona "pluralizar" términos en euskera al usarlos en castellano.
Así, la forma aceptada oficialmente es Gobela ([Euskaltzaindia Araua 0166](http://www.euskaltzaindia.eus/dok/arauak/Araua_0166.pdf)).
Sin embargo, también se indica que etimológicamente podría provenir de:

> Una clave para la Hidronimia Pirenáica
>
> GOBE. El paso de sima a río es tan  frecuente (mlo) que aún en pueblos guipuzcoanos he  oído decir a alguien que cayó
> al río que lo hizo al pozo, (potzura) y  se  dan en  el Gave bearnés que según Humboldt procede de la cava latina, y
> en el Gobe vasco occidental que se ve en Gobeo y Gobelats (puras geminaciones de arroyo) que se pueden relacionar ya
> con la cova latina.

> LA NATURALEZA EN LA TOPONIMIA ESPAÑOLA, V
>
> Gobe (C): ver Goibe
>
> Goibe (C): bajo la cima (goi-be)

Algunas fuentes como [LEIOAKO LEKU-IZENAK](https://www.euskaltzaindia.eus/dok/iker_jagon_tegiak/75756.pdf) de
Euskaltzaindia especifican:

> Gobela (ibaia, río)
>
> Conocido río que proviene de Urduliz.
> El étimo es completamente desconocido, pero tiene una historia singular, dado que Gobela era un barrio de Getxo que
> abarcaba los barrios de hoy en día Aiboa y parte de Neguri (hasta el río).
> En castellano es común añadirle un *-s* al final, por lo que alguna vez se ha pensado que el nombre original era
> *Gobelats* ya que *latsa* en euskera es ‘regato’.
> En la documentación se llama *kresaltsu*.

De acuerdo con [GOBELAKO BEGIAK (LOS OJOS DEL GOBELA)](http://getxosarri.blogspot.com/2013/03/gobelako-begiak-los-ojos-del-gobela.html) y [LA ELECTRA Y OTRAS CALLES DE ROMO](http://getxosarri.blogspot.com/2015/04/la-electra-y-otras-calles-de-romo.html),
*Kresaltsu* puede ser el nombre que tenía el Gobela(s) en Leioa y algunas partes de Getxo, lo que conocemos como Romo.

¿Podríamos imaginativamente suponer que Gobela es *"el arroyo bajo la cima (de las peñas que tienen agua)"*?
¿Pudiera ser que el origen del nombre del río Goibelats esté en Urdunlaitz y que el caserío de Algorta tomara su nombre
del río y no al revés?
Parece consistente con algunos vecinos que dicen haberse referido siempre al arroyo en Urduliz como Gobelas.
Pero a falta de una fuente documental más sólida, lo tomaremos como una licencia para dar épica a nuestra aventura.

```{note}
Otro artículos de getxosarri.blogspot.com interesantes sobre el Gobela(s):

- [LOS PUENTES DEL GOBELA -I-](https://getxosarri.blogspot.com/2013/10/los-puentes-del-gobela-i.html)
- [CAMBIOS DE CURSO Y PUENTES EN EL GOBELA](https://getxosarri.blogspot.com/2015/01/cambios-de-curso-y-puentes-en-el-gobela.html)
```

## Búsqueda de referencias

De acuerdo con la página de la Diputación, el arroyo Muñarrikolanda procede de las peñas de Urduliz y desemboca en el
río Gobelas.

> [bizkaia.eus/es/tema-detalle/-/edukia/dt/2634](https://www.bizkaia.eus/es/tema-detalle/-/edukia/dt/2634)
>
> El río Gobelas, de 9 km de longitud, tiene su origen en una pequeña elevación, el monte Ganes (186 m) entre Sopelana y
> Barrika.
> Además del cauce principal se destacan los arroyos Muñarrikolanda, procedente de las peñas de Urduliz (207 m) y el río
> Bolue que, procedente del Monte Unbe (265 m), atraviesa el valle de Martiartu con un recorrido de unos 7 km hasta su
> encuentro con el Gobelas.

Sin embargo, en el [Visor de URA](https://www.uragentzia.euskadi.eus/informacion-del-agua/informacion-geografica-visor-gis/visor-gis/webura00-contents/es/)
(la Agencia Vasca del Agua) no existe el arroyo Muñarrikolanda (ver {numref}`VisorURA`).
De acuerdo con esta fuente:

- El río Gobela se forma por dos arroyos provenientes de Gane/Gana, entre Sopela y Barrika.
    - Lemotza, discurre por la misma cuenca que la calle/carretera Loiola Ander Deuna, hasta unirse con el Gobela a la
      altura de Urko.
    - Agirreko urak, recorre el límite donde coinciden Sopela, Barrika y Urduliz, al Oeste del poligono Igeltsera.
      Al cruzar las vías del metro, el nombre cambia a Gobela, que mantiene hasta la desembocadura en la ría.
- No hay ningún trazo/arroyo al norte del cordal, pero sí se indica *Azkarraiturri*, en el sureste del Hospital.
- De forma similar, es llamativo que se indica *Arriagako iturria* el Este del barrio Arriaga, pero no hay ningún arroyo.
- Al Sur del cordal de Santa Marina hay un arroyo, Zaituerreka, que recoge las aguas de Torreko serro, Alontsoeneko serro,
  Maneneko serro, Mendibeko serro, y otros arroyos sin nombre.
  Se une al Gobela a la altura de Urko, poco antes que Lemotza.
    - ¿Es posible que lo que la Diputación denomina Muñarrikolanda sea Zaituerreka?
      Si es el caso, sería recomendable sugerir a la Diputación que se refiera al cordal como las "peñas de Urduliz-Sopela",
      utilizando las expresiones "peñas de Urduliz" o "peñas de Sopela" cuando se pretenda hacer referencia sólo a la
      vertiente Norte o sólo a la vertiende Sur, respectivamente.
      La división entre ambos municipios es el cordal, y resulta confuso referirse a ellas como las de Urduliz sólo;
      aunque es cierto que parecen tener mucha mayor importancia en la vida diaria de los vecinos de Urduliz a juzgar
      por la atención que han prestado estas últimas décadas los Ayuntamientos.

(VisorURA)=
```{figure} ../_static/img/VisorURA.png

Captura del Visor de URA (Agencia Vasca del Agua).
```

Con respecto a las fuentes (*iturri*) en un [mapa toponímico municipal](https://www.geo.euskadi.eus/cartografia/DatosDescarga/Cartografia_Basica/Mapas_papel/Mapas_Toponimicos_Municipales/pdf/B_Urduliz_2012_E6500_ANV.pdf) vemos:

- Se muestra un arroyo al Este de Arriaga, pero no se indica la fuente *Arriagako iturria*.
  El arroyo se pierde en la calle Antsonekoa (al Norte de Igeltsera), por lo que puede que hoy en día también esté
  soterrado (y seco en la superficie).
- Se indica *Azkarraiturri* pero no se muestra ningún arroyo.


Parte de la información disponible en URA parece confirmarse en la descripción de [yt: El machacado cauce del río Gobela](https://www.youtube.com/watch?v=PsWEtuGge8c):

> El Gobela nace en Barrika, de las fuentes y manantiales (Agirreko urak) que surgen en la vertiente suroeste del monte
> Ganburutxu (174,36 metros) en el barrio de Arriaga.
> Después de marcar el límite de los municipios de Barrika y Sopelana, viene a hacer lo mismo con este último y Urduliz,
> hasta la altura de la fuente de Larrabeiti.
> Cruza el municipio de Sopelana de este a oeste por el sur de su casco urbano.
> Atravesando el mismo se le unen dos arroyos, primero el Zaituerreka (Muñarrikolanda) por la orilla izquierda y después
> el Lemotxa por la orilla derecha.

Es interesante que se dice que *Agirreko urak* surge de *"fuentes y manantiales"*, en plural, y el nombre en euskera
también está en plural.
Puede que ese arroyo no tenga un único punto de surgencia.
Sería interesante visitar la zona, aunque estrictamente sea el municipio de Barrika, y no las aguas que las peñas
desaguan al Norte.

El artículo *NUEVAS LOCALIZACIONES ARQUEOLÓGICAS AL AIRE LIBRE DEL PALEOLÍTICO INFERIOR Y MEDIO EN LAS CUENCAS DEL
GOBELA Y DEL UDONDO (BIZKAIA)* (
[Kobie_32_web.pdf](https://www.bizkaia.eus/fitxategiak/04/ondarea/Kobie/PDF/2/Kobie_32_web.pdf?hash=c51c6ced087197ebb149a64ddd3c8a1a),
[PDF_139_Rios-et-al.-2013-Kobie.pdf](https://www.arteprehistoricoiiipc.unican.es/wp-content/uploads/2021/07/PDF_139_Rios-et-al.-2013-Kobie.pdf)
) parece, sin pretenderlo, darnos pistas.
La segunda sección dice:

> 2. LAS CUENCAS DEL GOBELA Y DEL UDONDO.
>
> El Gobela y el Udondo son dos ríos con características comunes.
> Presentan cauces cortos (4,19 km el Udondo y 9 km el Gobela), tienen sus cabeceras en colinas de baja altitud, se
> alimentan de las aguas vertidas por numerosos arroyos de escaso caudal, forman humedales en sus tramos medios y
> finales debido a la escasa altitud respecto al nivel del mar, presentan modificaciones importantes de sus trazados por
> acción humana y desembocan actualmente en la ría del Ibaizabal-Nerbioi aunque originalmente el Gobela lo hacía
> directamente en el Abra.
> Hoy en día ambas cuencas están afectadas, de manera desigual, por la intensa urbanización causada por una población
> total de unos 150.000 habitantes, repartida entre Getxo, Leioa, Berango y Sopela.
> Estas dos cuencas presentan además algunas peculiaridades originadas por un complejo modelado del terreno en época
> cuaternaria.
> El Gobela nace en el alto de Gane (175 m), alimentado por varios arroyos entre los que destaca el Lemotza.
> Rápidamente se encaja contra Frailemendi, discurriendo a partir de este momento en paralelo a la costa hasta
> desembocar originalmente entre los barrios de Neguri y Las arenas donde formaba un potente arenal.
> Desde la margen derecha del Gobela hacia la costa el terreno asciende significativamente, hasta un máximo de
> +65 m.s.n.m en Alango.
> La costa actual, por su parte, se eleva unos 20-30 m respecto al mar a partir de Punta Begoña, formando acantilados
> que alcanzan alturas de +65 m en La Galea y de +85 m sobre la playa de Bariñatxe.
> En varios puntos sobre estos acantilados se localizan arenales como los descritos sobre la playa de Barinatxe (Muñoz
> et al 1991).
> La margen izquierda del Gobela se alimenta de los arroyos de Zaituerreka e Iltzaerreka que nacen en Munarrikolanda
> (254,77 m); de Bolue que lo hace en Unbe (298,83 m) y el de Kurkudi que nace en el promontorio homónimo (125 m).
> Hay que destacar que en la confluencia del Bolue y el Gobela se forman unos amplios humedales (Bolue y Fadura) y
> que en la zona de Las Arenas hubo en tiempos una amplia zona de marismas.

(Cuencas)=
```{figure} ../_static/img/Cuencas.png

Figura 1 del artículo *NUEVAS LOCALIZACIONES ARQUEOLÓGICAS AL AIRE LIBRE DEL PALEOLÍTICO INFERIOR Y MEDIO EN LAS CUENCAS
DEL GOBELA Y DEL UDONDO (BIZKAIA)*.
```

Dado el rigor que se espera de una publicación científica, y en vista de lo detallada que es la descripción, resulta
muy llamativo que no se mencione Urduliz entre los municipios en la cuenca del Gobela.
¿Por qué?
La respuesta parece estar en la Figura 1 del mismo artículo (ver {numref}`Cuencas`).
El área delimitada por la línea de puntos se entiende que se corresponde con las cuencas del Gobela y el Udondo, que es
sobre lo que trata el artículo.
A primera vista, cabría pensar que coincide parcialmente con el límite municipal entre Urduliz y Sopela, por lo que todo
Urduliz se encuentra fuera de la cuenca del Gobela.
Sin embargo, la figura no es del todo precisa porque hay algunas zonas costeras que desaguan directamente al mar, en vez
de caer hacia el Gobela(s).
¿Puede ser que se hayan tomado ciertas licencias para no complicarse en exceso y centrarse en su contenido (que son los
restos arqueológicos y ninguno de los que exponen está en Urduliz)?

(CuencasURA)=
```{figure} ../_static/img/CuencasURA.png

Captura del Visor de URA (Agencia Vasca del Agua) habiendo activado la capa *"Cuencas vertientes a masas de agua superficial"* (verde).
```

Volvemos al Visor de URA, y activamos la capa *"Cuencas vertientes a masas de agua superficial"*, además de los límites
municipales (ver {numref}`CuencasURA`).
¡Sorpresa!
Urduliz reparte sus aguas en tres cuencas:

- *Gobelas-A* al Oeste.
- *Butroe drenaje transición* en el centro y norte.
- *Butroe-B* desde el noreste hasta el sureste.

Si miramos más en detalle la cuenca *Gobelas-A* en Urduliz ({numref}`CuencaGobelasAUrduliz`), ¡premio!
Desde Lukiatxa hacia el Oeste, los barrios de Goieta, Mauratza, el Hospital, La Campa, todas las nuevas construcciones
en la calle Arteta y prácticamente todo el polígono Igeltsera desaguan hacia el Gobelas.
Por lo tanto, es muy probable que en su día hubiera uno o varios arroyos que se encontraran con el Gobela en algún punto
entre las cocheras del metro y el encuentro de la calle Arteta con la calle Olabide en Sopela.
Que el cambio de nombre de *Agirreko urak* a *Gobela(s)* se de en ese punto en concreto está muy probablemente motivado
porque ahí es donde concluían dos arroyos (el proveniente de Barrika, habiéndose unido al menos dos arroyos menores, y
el proveniente de Urduliz, habiéndose unido probablemente tres arroyos menores).

(CuencaGobelasAUrduliz)=
```{figure} ../_static/img/CuencaGobelasAUrduliz.png

Captura del Visor de URA (Agencia Vasca del Agua), detalle de la cuenca *Gobelas-A* en Urduliz.
```

```{note}
El [visor cartográfico de la Diputación](https://www.bizkaia.eus/es/cartografia-y-ortofotos) tiene ortofotos desde el
año 1956, anuales a partir de 2005.
Ver, por ejemplo, {numref}`Ortofoto1956`.

Sería muy interesante descargarlas todas y solaparlas, para apreciar los cambios.
En las de los 50 y 60 se ven algunos de los caminos/senderos tradicionales que recuerdan los vecinos.
Concretamente, hablando con dos vecinas de Torrebarria sobre cómo llevaban las vacas a Berango o Algorta, recordaban
caminos ligeramente diferentes, atravesando las laderas al noroeste del cordal a diferentes alturas.
También pueden intuirse los cauces originales.

Al margen de los caminos del agua, estas imágenes pueden servir para recuperar senderos y calzadas tradicionales que
pueden estar cubiertas o desfiguradas por la vegetación y movimientos de tierras.
```

(Ortofoto1956)=
```{figure} ../_static/img/Ortofoto1956.png

Ortofotografía de 1956.
```

## Puntos concretos de desagüe al Gobela(s) desde Urduliz

El 4 de septiembre de 2023 llovió a última hora de la tarde.
Aprovechando que cuando agua lleva el río suena, visité la zona entre el Garbigune y la calle Arteta.
Al día siguiente, visité y hablé con varios vecinos de Elizalde, Torrebarri, Azkarre y Santamariñealde.
El 7 de septiembre, visité y hablé con vecinos de Iturriaga e Iturri-beheko, y también con una vecina de Marusas que
creció junto a la Iglesia en La Campa.
He hablado con entre un tercio y la mitad de casas/caseríos de la zona, charlando con vecinas y vecinos de entre sesenta
y más de ochenta años.
He tenido como compañeros de aventura a vecinos en sus cuarenta.
Las siguientes secciones reflejan el camino que he seguido estos días desde el Gobelas hacia arriba (hacia el este
sureste y después al sur para enfilar las peñas), y cómo he ido completando la información conforme los vecinos han ido
aportándome detalles.

### Unión con Agirreko urak

En el extremo noroeste de la parcela 7001 hay una salida proveniente de Urduliz que se une a *Agirreko urak* por su
orilla izquierda, el cual llega soterrado del otro lado de las vías y se descubre en el mismo punto que cambia de nombre
y confluye con el desagüe de Urduliz.

::::{card-carousel} 2

:::{card}
![](../_static/img/umarcor/GobelasAgirrekoUrak.jpg)
:::

:::{card}
![](../_static/img/blanca/GobelasAgirrekoUrakCaudal.jpeg)
:::

:::{card}
![](../_static/img/blanca/GobelasAgirrekoUrakCaudales.jpeg)
:::

:::{card}
![](../_static/img/blanca/GobelasAgirrekoUrakHormigon.jpeg)
:::

:::{card}
![](../_static/img/blanca/GobelasAgirrekoUrakPuertaFrontal.jpeg)
:::

:::{card}
![](../_static/img/blanca/GobelasAgirrekoUrakPuertaCenital.jpeg)
:::

:::{card}
![](../_static/img/blanca/GobelasAgirrekoUrakSuciedad.jpeg)
:::

::::

```{note}
Algunas de las fotografías anteriores se han tomado accediendo desde la calle Elizalde.
No puede accederse desde Sopela siguiendo la vía del metro, ya que el vallado de la vía se ensancha para hacer sitio a
un canalón, y eso impide salvar los 1-2 m:

::::{card-carousel} 1

:::{card}
![](../_static/img/umarcor/GobelasAgirrekoUrakPaso1.jpg)
:::

:::{card}
![](../_static/img/umarcor/GobelasAgirrekoUrakPaso2.jpg)
:::

::::
```

Asimismo, apenas diez metros aguas abajo, hay una instalación del Consorcio de Aguas (visible también en las imágenes
de satélite) con evidente ruido de aguas en su extremo oeste (el cauce discurre de este a oeste en ese tramo).

Para llegar a la instalación, hay que seguir un sendero que surge de la calle Elizalde de Urduliz, a la altura de una de
las entradas/salidas del pequeño aparcamiento con la instalación de gas natural:

::::{card-carousel} 2

:::{card}
![](../_static/img/umarcor/GobelasInstalacion.jpg)
:::

:::{card}
![](../_static/img/umarcor/GobelasCaminoInstalacion1.jpg)
:::

:::{card}
![](../_static/img/umarcor/GobelasCaminoInstalacion2.jpg)
:::

:::{card}
![](../_static/img/blanca/GobelasInstalacionSendero.jpeg)
:::

::::

Por lo tanto, es posible que la pequeña salida proveniente de Urduliz sea un desagüe menor, y sea esta instalación la
que desagua todas las aguas provenientes de Marusas.
La instalación está centrada en el límite municipal de Sopela y Urduliz, ocupando parte de la parcela 6002 de Sopela y
parte de la parcela 1002 de Urduliz.

```{note}
Es llamativo que en el sendero que atraviesa la parcela 1002 hay una "puerta" a mano izquierda, con una cadena en un
estado relativamente reciente:

![](../_static/img/umarcor/GobelaCaminoInstalacionPuerta.jpg)
```

A continuación de la instalación, el Gobela(s) pasa bajo la carretera Olabide para cruzar al polígono donde se
encuentran las cocheras:

![](../_static/img/umarcor/GobelasPasoMetro.jpg)

Avanza con la carretera en su orilla izquierda hasta volver a cruzarla soterrada a la altura del encuentro con la calle
Arteta.
A partir de ese punto, sigue con la carretera Olabide en su orilla derecha hasta llegar al final de la misma, ya muy
dentro del municipio de Sopela.

```{note}
Las casas/caseríos en Elizalde siguen teniendo pozos independientes, no tiene un sistema de saneamiento conectando las
mismas, ni con lo que provenga de Marusas.
Sin embargo, conforme la urbanización avance a lo largo de las vías del tren, y también a lo largo de la calle Arteta,
es de esperar que en la próxima década acaben integrándose, como se dejaron de usar los pozos que también había en el
barrio Marusas hasta hace poco.
```

### Cruce de las calles/carreteras Arteta y Olabide

En el encuentro de la calle/carretera Arteta de Urduliz con la calle/carretera Olabide de Sopela, donde el Gobelas
hace el segundo paso soterrado, en el otro lado de la intersección, al Este de la misma, el terreno parece representar
perfectamente el cauce de un arroyo; apreciable en las fotos de satélite.
Sin embargo, no se oye agua correr.
Aunque el continuo paso de coches dificultaban la tarea, en otros puntos sí se oye río claramente.
Por otro lado, no parece haber ningún paso por debajo de la carretera/intersección, ni de entrada ni de salida.
Ver parcelas 2011 y 2014 de Sopela y parcela 12001 y el extremo suroeste de la 7001 de Urduliz:

![](../_static/img/umarcor/CruceArtetaOlabide.jpeg)

En este sentido, el siguiente testimonio de un vecino parece relevante:

> Hemos comprado casa en las torres frente al hospital y uno de los inconvenientes que se encontraron al principio fue
> una tubería de más de 50 años cruzando por medio de la campa.
> Actualmente estaba en desuso, pero puede que hace años redirigieran por ahí el arroyo.
> Si te sirve como punto de partida, se que el consorcio de aguas metió mano, pero no se hasta que punto ya sabían de su
> existencia o si se actuó en ella tras el descubrimiento.

Detalle de desagüe en las últimas construcciones de la calle Arteta, que dirige aguas a la ladera que termina en el
regato:

![](../_static/img/umarcor/DesagueArteta.jpeg)

Aunque parece un desgüe menor, esas aguas van a parar al regato que termina en noroeste del cruce entre las carreteras
Olabide y Arteta.

Es probable que en la construcción de la calle Arteta se capturaran parte de las aguas que trazaron la orografía visible
en las fotos de satélite.
Si hay una canalización significativa bajo la calle Arteta, sería interesante localizar el punto donde esa canalización
desagua al Gobela(s).
Puede que sea debajo de la intersección/carretera, o tras la salida del Gobelas al sur de la intersección.

Los vecinos de la zona dicen que tradicionalmente no ha habido ningún arroyo ahí, pero sí un regato los días de lluvia.
Los que recordamos el trazado de la calle/carretera Olabide antes de que se hiciera el cruce con la calle Arteta (cuando
era una curva de 90º) sabemos cómo el agua rebasaba esa curva los días de lluvia, significativamente notable en
comparación con la misma carretera en metros anteriores o posteriores a la curva.

El 11 de septiembre volví a visitar la zona con mi ama, y pudimos ver el canal que atraviesa la calle Olabide al Norte
del cruce con la calle Arteta.
La maleza confunde la entrada al canal/tubo con el "muro" que sostiene la propia acera, pero apartándola con un paraguas
un día de lluvia pudimos ver cierto caudal.
La salida, en el lado del polígono es más evidente.
Es decir, el regato atraviesa la calle Olabide en su tramo perpendicular a la calle Arteta, para inmediatamente después
volver a atravesar la calle Olabide, ya habiéndose unido al cauce principal del Gobela(s).

::::{card-carousel} 2

:::{card}
![](../_static/img/blanca/CanalArtetaEntrada.jpeg)
:::

:::{card}
![](../_static/img/blanca/CanalArtetaSalida.jpeg)
:::

:::{card}
![](../_static/img/blanca/CanalArtetaSalidaBlanca.jpeg)
:::

::::

```{attention}
Varios vecinos de las parcelas inmediatamente al norte-noreste de este cruce me han comentado que hay jabalíes y corzos
en ese bosquecillo.
Hasta la construcción de la calle Arteta, los animales podía subir hacia el cordal y moverse por el mismo, pero ahora
ese bosque se ha quedado aislado.
O no, y ese es el problema.
Si las poblaciones están confinadas en ese bosque, podrá haber problemas de endogamia y/o eventual desaparición, pero
si no están confinadas y cruzan la carretera, puede haber accidentes graves.
Actualmente la velocidad en esa vía es excesiva para el riesgo de atropello.
```

### Fuente de Larragoiti

En la descripción del vídeo citada anteriormente se menciona la fuente de Larrabeiti, aunque no la hemos encontrado
indicada en ningún mapa.

En relación con la carretera Elizalde que baja de La Campa a las cocheras del metro, una vecina e irakasle me comentó en
su día que eso fue tradicionalmente un camino de vacas para bajar a beber a la fuente que había abajo, que luego se hizo
carretera y que por eso pasaba entre dos casas de forma tan poco conveniente.

Basándome en eso, otra salida natural de Urduliz al Gobelas puede ser una surgencia en el pequeño parking que hay
en la base de esa carretera, en el extremo del parking que pertenece a Urduliz (ya que la mayoría es de Sopela).

::::{card-carousel} 1

:::{card}
![](../_static/img/umarcor/AparcamientoElizalde.jpeg)
:::

:::{card}
![](../_static/img/umarcor/FuenteLarragoiti.jpeg)
:::

::::

Sin embargo, al visitarlo un día de lluvia no pude diferenciar si lo que estaba escuchando entre la maleza era un
pequeño manantial o la lluvia y los árboles moviéndose.

El 5 de septiembre hablé con varios vecinos de las parcelas 1006, 1003 y 1004.
Me confirmaron no sólo la existencia de la fuente/manantial sino la importancia que tuvo para todos los caseríos de
la zona.
Se referían a ella como la fuente de Larragoiti, ya que en su día *"había un gran caserío ahí abajo donde vivían por lo
menos 5 o 6 familias"*.
La fuente la usaban como abrevadero, lavadero (para aclarar prendas pero también para limpiar las tripas después de la
matanza del cerdo), y los niños para coger zapaburus.
Curiosamente, los vecinos de cada parcela usaban senderos ligeramente diferentes para acceder.
Todos atravesaban las parcelas 2007 y 2001, pero los trazados eran los más directos en cada caso, reservando lo que
ahora es la carretera para bajar/subir con ganado.

Todos me confirmaron que la fuente *"está ahí mismo"*, pero las últimas veces que han ido la maleza estaba muy cerrada y
no se han puesto a abrir camino.
La opción más fácil parece ser un parking temporal que hubo entre la calle Elizalde y la calle Arteta, en la parcela
2011.
Se trata de un parking de grava oscura (óxido de hierro) que acabó usándose como vertedero, por lo que cerraron el
acceso y la vegetación lo ha ocultado.
Sin embargo, la vegetación no ha cubierto lo que fue el parking, sólo está oculto tras una fina línea de maleza en el
arcén de la carretera:

![](../_static/img/umarcor/ElizaldeParkingGravaOscura.jpeg)

Queda pendiente por tanto ir con tijeras de podar y buscar en el extremo oeste del parking.
En caso de encontrar la fuente, es posible que esté seca hoy en día, debido a todos los nuevos bloques que se han
construido y se están construyendo en la calle Arteta, particularmente cerca del Hospital.
Pero, si la fuente/manantial recoge principalmente las aguas del alto de La Campa, podría tener caudal.
Alternativamente, si el desnivel con respecto al Hospital es suficiente como para que las aguas del manantial sean más
profundas que los cimientos de las construcciones recientes, podría tener caudal.

No está claro si la fuente está en la parcela 2011 (Sopela) o en la 2001 (Urduliz), ni quiénes son los propietarios.
Muy curiosamente, la parcela 7001 tiene un sendero recto que separa la parcela 2001 de la 12001.
La parcela 7001 son todas las calles públicas del centro de Urduliz.
El sendero parece terminar exactamente en la linde entre la parcela 2011 y 2001.
Puede que precisamente ahí esté la fuente.

Sea como fuere, esta fuente no aparece en ninguno de los mapas de URA o de la Diputación, a diferencia de Azkarre o
Arriaga que sí están en algunos (aunque no en todos).
Sería interesante recuperarla, incluirla en algunos de los itinerarios/paseos del pueblo y visibilizarla en la cartografía.

## Puntos concretos de surgencia/acumulación

### Fuente de Azkarrez/Azkarre/Azkarra

El 5 de septiembre, tras hablar con los vecinos de Elizalde, hablé con los de Torrebarria.
Además de confirmarme la información sobre la fuente de Larragoiti y el regato de la calle Arteta, hicieron hincapié
en la fuente de Azkarrez, y en la cueva de Santamariñealde (aunque no dieron nombre a la cueva, sólo describieron su
ubicación en referencia a los vecinos de la parcela donde se encuentra).
Hablando con más vecinas en días posteriores, todas señalaban ambas fuentes (cómo tenía lavadero, además de zapaburus) y
la cueva.

Mi percepción es que la fuente de Azkarrez fue más importante para una mayoría que la de Larragoiti porque se encuentra
más cerca y con mucho menos desnivel.
Me comentaron que aunque la fuente de Azkarrez está en propiedad particular, era habitual que llevaran las vacas a su
abrevadero, ya que el terreno no estaba cerrado/vallado (hoy sí lo está, principalmente porque el Hospital está
construído casi sobre la fuente; habrá 5-10 m entre la pared del Hospital y la fuente).
Yo estaba centrándome aguas abajo de su ubicación, y agradezco que redirigieran mi atención aguas arriba también.

Tras una agradable charla en Torrebaria, me dirigí a Azkarrez, que se encuentra en el extremo sur del Hospital, ya que
es la parcela colindante y la casa está en el oeste de la parcela.
De hecho, conozco el caserío de toda la vida ya que jugué y estudié con el hijo de Ainhoa, pero desconocía el nombre del
caserío, y desconocía (o no recordaba) que tuviera una fuente.

Ainhoa me atendió con mucho gusto.
Me mostró el abrevadero, que es visible desde la puerta, y el aljibe junto a él.
Me explicó que del muro del aljibe salían chorros de agua, y que debajo solía haber un lavadero, ya que además de traer
a los animales, también lavaban la ropa.
Describió el tamaño del aljibe *"como una casa de grande"*.
Aunque aun así me comentó que a veces en verano se seca, cuando llueve, el agua tiene mucha fuerza.

Como anécdota, me comentó que cuando construyeron el Hospital hicieron el límite de la parcela 1-2 m más al Este que
ahora.
Ella les dijo que no era buena idea, porque estaban poniéndolo por donde cae el agua con fuerza, aunque en ese momento
no tuviera apenas caudal.
No le hicieron caso, y en las primeras lluvias el agua arrastró el vallado.
Después, dice, *"trajeron una máquina impresionante y construyeron el muro reforzado de hormigón"* que se ve ahora,
desplazado 1-2 m de forma que el arroyo queda fuera de la parcela del Hospital.

::::{card-carousel} 1

:::{card}
![](../_static/img/umarcor/MuroAzkarre.jpeg)
:::

:::{card}
![](../_static/img/blanca/HospitalMuroAzkarre.jpeg)
:::

::::

```{note}
Varios vecinos recuerdan que cuando se hizo el Hospital hicieron una recanalización impresionante, tanto que entra una
persona de pie en el ducto subterráneo.
No estoy seguro de si la obra se hizo al mismo tiempo que el Hospital, o justo antes, en preparación para el mismo.
Pero los vecinos lo recuerdan como un todo.
Nótese que es la tercera obra significativa de la que tenemos constancia, después de la de finales de los 90 (o primeros
de los 2000) y después de la canalización inicial (que asumimos se hizo a primeros de los ochenta o a finales de los
setenta).
```

Ainhoa, también me comentó que un resto de cemento/cola que hay en el lateral del abrevadero es de una placa de piedra
que alguien puso indicando que era el nacedero del Gobela(s).
Placa que alguien arrancó.
No sabe ni quién/por qué la puso, ni quién/por qué la quitó.

![](../_static/img/blanca/FuenteAzkarre.jpeg)

En cuanto al topónimo, he encontrado referencias como Azkarrez, Azkarre o Azkarra.
Ni siquiera recuerdo cuál en concreto usaban los vecinos.
En cualquier caso, parece que etimológicamente pudiera proceder de Aitz y Erreka, el río de las peñas.
Por lo tanto, a falta de inspeccionar el extremo Norte (el más alto) del aljibe, parece que sí podría ser el punto de
concentración de caudal en superficie en cota más alta dentro del trazado del arroyo (o este brazo del mismo).

El 7 de septiembre por la mañana, hablé con el matrimonio de *Iturri-beheko*, las personas de mayor edad con las que he
hablado en esta aventura.
Corteses preo más desconfiados que otros, hablamos brevemente a través de la ventana de la cocina.
El hombre fue tajante cuando les comenté que estábamos intentando entender el curso de las aguas de las peñas al Gobelas:
*"Hazme caso a mí: el nacimiento del Gobelas está en Azkarre, ahí he cogido anguilas yo"*.
Le pregunté si conocía o recordaba alguna otra fuente/surgencia/manantial hacia el Este.
Me dijo que no hay ninguna otra fuente hasta el cementerio nuevo.

```{important}
Tengo entendido que todas las fuentes/manantiales deben tener acceso público, aunque se encuentren en propiedad privada.
Por lo tanto, es posible que el Ayuntamiento tenga que acometer una obra para recolocar la valla de acceso a Azkarre, de
forma que se encuentre justo después de la fuente y no antes.
De hecho, por referencias de todos los vecinos, la fuente de Azkarre siempre a sido de uso público.
Es posible que la valla se pusiera al hacerse el Hospital, y que no fuera explícitamente solicitado por los propietarios.
```

### Cueva de Santamariñealde

Los vecinos de Torrebarria y la vecina de La Campa hicieron mucho hincapié en una cueva que existía en la parcela 0148.
Solían llevar las vacas allí, y tenían que meterse en la cueva con una manguera para sacar agua.

Me hablaron de Trini y de su hijo Gorka, o de Gorka y su ama Trini.
Tras hablar con Ainhoa, toqué el timbre de la parcela 0148.
Salieron dos personas, una madre y un hijo.
Al decirles que buscaba a Gorka o Trini, y que suponía serían ellos, por referencias de algunos vecinos, Gorka se acercó
para otra muy agradable e interesante conversación.

Me explicó que la cueva se cayó hace años, cuando se derrumbó el sector Parking también.
Lo cierto es que he oído varias referencias al respecto, pero no tengo claro cuándo fue, aunque mi hermano dice que hace
relativamente poco.
Yo no recuerdo un cambio muy notable desde que yo era pequeño, pero también es cierto que no tengo ningún recuerdo claro
de esa pared/tramo del cordal.
He encontrado la siguiente fotografía en [verpueblos.com/pais+vasco/vizcaya/urduliz/foto/79521/](https://www.verpueblos.com/pais+vasco/vizcaya/urduliz/foto/79521/), por lo que si parece que el desprendimiento fue brutal:

![](../_static/img/ParkingVerPueblos.jpg)

No me quedó del todo claro si la cueva se cerró completamente, o si está muy peligroso/inestable como para entrar.
En cualquier caso, al preguntarle sobre el tamaño, me contó que es/era más una gatera que una cueva como tal.
También me explicó que el nivel del agua variaba a lo largo del año, y que por eso había que meterse más o menos
para poner el extremo de la manguera.

Mientras conversaba con él, la teoría que estaba en mi cabeza es que hay ciertas capas impermeables bajo las peñas que
producen la creación de pozos subterráneos, y que la cueva/gatera de la que estábamos hablando es un acceso por arriba
a uno de esos pozos; mientras que la fuente de Azkarrez sería una salida lateral o inferior de ese mismo u otro pozo
similar.
Cuando Aitor me sugirió activar la capa *"Sectores de las masas de agua subterráneas de la CAPV"* del visor de URA, no
lo relacioné, pero en este contexto la siguiente captura puede ser relevante:

![](../_static/img/MasasSubterraneas.png)

En algún informe geológico sobre el área, que mayormente no he entendido por el vocabulario, me ha parecido comprender
que puede haber desplazamientos laterales de agua entre paredes/techos de piedra mas/menos permeable.
No obstante, es posible que un geólogo se ría de mí porque esas masas de agua estén a tanta profundidad que sea ridículo
relacionarlo con los arroyos.
Me debato entre eso, y que es mucha casualidad que las fuentes de Azkarrez y Larragoiti y el origen de *Agirreko urak*
estén ambas cerca de las masas subterráneas.

Otras posibles relaciones que me vienen a la cabeza son los abrigos/refugios de Erdikoatxa y Lukiatxa, y la cueva que
se referencia en los documentos históricos revisados por Koldo Somocueto.
Los abrigos/refugios podrían haber existido antes de que nacieran quienes ahora tienen hasta 85 años.
Por lo tanto, aunque recuerden haberlos usado como abrevadero toda la vida, pueden ser artificiales y recientes.
Por otro lado, si se trata de una abertura natural, que da a un pozo natural, surgen muchas preguntas, sobre por qué
no se referencia en ningún documento histórico, o por qué nadie la ha explorado.
Al preguntar a vecinos sobre si la cueva era natural o artificial, parecen seguros de que era natural y había estado ahí
toda la vida.
Pero en las ortofotos de los 50 parece que había una cantera si no activa, reciente.

Sería interesante poder acudir con alguien más (puede que Ander) para conocer la ubicación exacta y poder solaparla con
ortofotos de diferentes épocas.
Pedir permiso a Gorka para descolgarnos desde el cordal en su parcela, para que ate a los perros un rato, y poder sacar
alguna foto de lo que pueda quedar de la gatera o de su ubicación, aunque sólo sea para publicar una versión reducida de
este contenido en la revista de Urduliz o Hiruka.
Puede que sin entrar pero usando un palo podemos sacar alguna foto del interior.
La razón de descolgarnos desde el cordal (y después yumarear), en vez de acceder andando, es evitar abrir sendero al
norte del cordal en ese tramo.
Hablando con Gorka sobre habilitar el cordal para que senderistas puedan recorrerlo, me ha dicho que no le importa,
siempre que no se metan en su parcela.
Y no es tanto por la molestia, sino porque tiene perros sueltos, y son perros protectores como en todos los caseríos.
Además de los perros, me ha contado una anécdota sobre hace varios años que alguna administración abrió pista en la
parcela colindante al oeste y tiempo después escucharon gritos de auxilio en su parcela.
Resultó que un ciclista había recorrido la pista de la parcela contigua y después había decidido intentar atravesar la
suya, aunque estaba sin limpiar.
Cayó en un agujero de algo más de un metro y se le cayó la bici encima, por lo que además del esfuerzo de estar
atravesando el zarzal tuvo que salir del atolladero.
Gorka y Trini lo oían pero no lo veían, precisamente por la maleza y porque ese tramo no es transitable.

### Charca de Lukiatxa

Uno de los abrigos del cinturón de la memoria (el que se encuentra más al Oeste de Lukiatxa, antes de Erdikoatxa) está
colmatado y eso ha llevado a pensar a algunos vecinos que pueda tratarse del punto de nacimiento de un arroyo.
Sin embargo, por las investigaciones realizadas cuando se realizaron las excavaciones del Cinturón de Hierro, había una
balsa de madera en su interior para poder almacenar víveres independientemente del nivel del agua.
Además, el agua está podrida por el estancamiento.
Por lo tanto, se puede concluir que esa sección de roca no es permeable, y no filtra hacia abajo.

Debajo de ese abrigo, en la cantera al norte entre Erdikoatxa y Lukiatxa se forma una balsa de agua
({numref}`ErdikoatxaCharco`).
Esa sección del cordal se encuentra muy modificada por la cantera (tanto que despareció un nido de ametralladora que se
encontraba en ese tramo), pero por la orografía que lo rodea, es probable que originalmente hubiera un arroyo entre las
parcelas 0038 y 0036, que descendiera entre las parcelas 0039 y 0059 para recorrer la calle Goieta (de abajo),
pasar por la Hípica de la parcela 0069 y ahí a Marusas.

Junto con la fuente de Azkarrez, es la pista más interesante para contrastar con la información que podamos obtener de Urbanismo y del Consorcio de Aguas.
Si la impermeabilidad de la roca mantiene la balsa gran parte del año (y cuando llueve el agua sale a chorro de las
paredes, como cuentan algunos vecinos), esa balsa (y no el abrigo/refugio de encima) podría ser uno de los dos
nacimientos del Gobelas en Urduliz, que se unen en la canalización subterránea en marusas.

(ErdikoatxaCharco)=
```{figure} ../_static/img/ErdikoatxaCharco.jpg

Charco entre Erdikoatxa y Lukiatxa al norte.
```

## Trazados y toponimia

Parece confirmado, por tanto, que Urduliz también contribuye a las aguas del Gobela(s), en igual o mayor medida que
*Agirreko urak* en el punto donde el Gobela(s) toma su nombre; y probablemente en al menos otro punto.
La leyenda local tiene algo de cierto.
Pero no es preciso que el nacimiento del Gobela(s) esté en las peñas.
Estrictamente toma su nombre en Urduliz, pero en el punto donde un arroyo proveniente de las peñas se une con *Agirreko
urak*.

Sería muy interesante descubrir qué nombre tomaba el arroyo en Urduliz antes de que dejara de ser considerado parte de
la red fluvial y pasara a ser parte del sistema de saneamiento.
A ese arroyo algunos vecinos se han referido como Marusas, pero no sé si porque estábamos hablando en términos actuales,
o porque también lo llamaban así antes.
Otros dicen haberlo llamado siempre Gobela(s), por lo que podría ser que el origen del nombre del río fuera este arroyo
de Urduliz.
Nos queda también por averiguar dónde empieza (altitud, cercanía a las peñas) y qué trazado tiene la canalización
subterránea.

Con respecto al trazado, por la experiencia con las riadas puntuales en Marusas mencionadas anteriormente, sabemos que
el camino natural del agua viene por el norte del Hospital, atravesando la calle central de Marusas, y la diagonal que
pasa por el Arbelaitz, para acabar en la ahora rotonda pintada donde se encuentra con la carretera que baja de La Campa.
Es posible que las dos calles que atraviesan el barrio de este a oeste fueran trazadas sobre el cauce del arroyo que
soterraron.
Sin embargo, en las siguientes fotografías de 1987 no se aprecia el arroyo.
¿Puede que ya se hubiera hecho la primera canalización?

![](../_static/img/blanca/CentroMarusas1987.jpeg)

![](../_static/img/blanca/CentroMarusas1987_2.jpeg)

Asumimos que esa misma canalización continua en paralelo a las vías del tren hasta su encuentro con el Gobela.
Hablando con un vecino que trabajó en la construcción, además de confirmar que la canalización es tan grande como para
caminar por dentro, me explicó que en su día hubo problemas con el caserío que quedaba junto a la rotonda, ya que en
cierta manera la canalización venía a salir a la puerta de su casa.

![](../_static/img/blanca/CaserioBajoMarusas1987.jpeg)

Recuerdo que ese caserío había quedado varios metros por debajo de las carreteras.
Siempre había estado un poco más bajo, y cuando llovía mucho se le inundaba el terreno, aunque normalmente no tanto como
para inundar la casa, pero tampoco era excepcional.
Pero conforme se fueron haciendo construcciones y rehaciendo las carreteras, acabó hundiéndose más.
Finalmente ese caserío fue derruido y ahora el terreno tiene la misma altura que las carreteras, por lo que entiendo que
se terminó de soterrar la canalización ampliada y se cubrió.

La unión con Agirreko urak debería ser el final de la canalización.

```{note}
La cercanía de la urbe al cordal en Urduliz parece no permitir arroyos naturales con caudal notable permanente, por lo
que el impacto de su soterramiento es probablemente irrelevante en comparación con la red de saneamiento artificial;
razón por la cual la integración se desdibuja conforme crece el núcleo.

Existe un arroyo al sur del cordal porque la densidad de viviendas es menor, pero aún así la mayoría de
afluentes del Zaituerreka provienen de Munarrikolanda, que tiene más ladera y distancia a la cumbre (por lo que se
entiende que el arroyo tome también esa denominación).

Sin embargo, eso no siempre fue así.
Dada la orografía, antes de que se canalizaran, debió haber al menos tres arroyos o regatos que confluían en el sureste
de Marusas, puede que varios de ellos más estacionales que permanentes pero todos necesarios en caso de lluvias (cuando
la capacidad de retención de las peñas se rebasa).

Parece que para resolver las incógnitas necesitamos acceder a los planos de alcantarillado del municipio, hablar con
alguien del Ayuntamiento que pueda estar al tanto de las actuaciones realizadas en este sentido, o consultar al
Consorcio de Aguas.
```

```{note}
La mayoría de bloques de viviendas de Marusas se hicieron a mediados de los 80.
Una de ellas se finalizó en 1987.
Es probable que un vecino de uno de esos bloques (que además, creo, ha tenido implicación en los planes urbanísticos del
municipio) tenga conocimiento de detalles sobre ese antiguo arroyo.
```

Mientras tanto, basándonos en la información de la que disponemos (fotos de satélite, cuencas de URA, curvas de nivel y
parcelario) podemos aventurarnos a dibujar los arroyos que pudo haber:

![](../_static/img/TeoriaArroyos.png)

Nótese que todos los trazos están dibujados a mano sobre las capas mencionadas:

- Línea verde gruesa: delimitación de la cuenca Gobelas-A de la capa *"Cuencas vertientes a masas de agua superficial"*
  del visor de URA.
  Está dibujada a ojo, porque no he conseguido descargar la capa para cargarla en QGIS.
- Línea verde fina: asumiendo que la altura de La Campa y el perfil de nivel de la calle Elizalde obligan a que existan
  dos subcuencas que desagüen al Gobelas, es la delimitación de ambas.
  Está dibujada completamente a ojo observando únicamente las curvas de nivel (y conociendo/recordando el terreno por
  ser de ahí mismo).
- Línea amarilla: río Gobela(s).
- Líneas magentas: suposición del trazado de los dos arroyos principales y los posibles subarroyos que alcanzaran el
  cordal.
  Dibujado a ojo, siguiendo patrones llamativos en las curvas de nivel.
  Nótese que el más largo sería el que llega al noroeste de Lukiatxa.
- Línea roja: suposición del trazado de un arroyo que proveniendo de Lukiatxa corresponde a la cuenca del Butrón.

```{note}
Preguntar a Aitor si existe algún plugin/script para, utilizando las curvas de nivel, modelar los trazados de las
subcuencas y los arroyos.
```

Tras hablar con varios vecinos, sabemos que el arroyo con dos subarroyos dibujado al oeste no es tal.
Es sólo un regato el que baja paralelo a la calle Arteta, y no existe el que baja del cordal (las lluvias caen por la
ladera al regato, ahora a la calle Arteta).
El regato paralelo a la calle Arteta es muy necesario los días de lluvia, pero no siempre tiene caudal.
También sabemos que el arroyo que cruza Marusas tiene su rama más occidental aún más al oeste de lo dibujado, pegado a
la linde este del Hospital.
Con respecto a las otras dos ramas, de Moruatxa Arista y Erdikoatxa-Lukiatxa, si observamos un [mapa toponímico municipal](https://www.geo.euskadi.eus/cartografia/DatosDescarga/Cartografia_Basica/Mapas_papel/Mapas_Toponimicos_Municipales/pdf/B_Urduliz_2012_E6500_ANV.pdf),
encontramos varias referencias llamativas:

![](../_static/img/MapaToponimico.png)

- Sólo hay una fuente indicada: *Azkarraiturri*.
- La zona al norte de Moruatxa es *Iturriagaburu*, y al noreste están *Iturriagaostea*, *Iturriagaurre*, *Iturrialde* e
  *Iturriondo*.
- Hay un caserío denominado *Iturriaga* y otro *Iturriagabarri*.
  *Iturriagabarri* tiene *Iturri-beheko* escrito en la entrada.
- Al norte están *Ibarzelaiak* y más al norte, cuando el terreno vuelve a ganar altura, *Goienalde*.

Por un lado, puede que la mayoría de los nombres tengan como base el apellido del caserío Iturriaga, por algún Iturriaga
que vino de otro pueblo con ese apellido, por lo que no tengan que ver con fuentes/manantiales/surgencias.
Pero es muy improbable.
El sufijo *'ga'* equivale al artículo en castellano:

> AGA, sufijo singular locativo, equivalente al artículo castellano EL o LA. Ejemplos: Osinaga, el pozo; Zuluaga,el hoyo;
> Aginaga, el tejo; Muruaga, o su variante Buruaga, el cerro Laga, etc.

y varios de los topónimos ni siquiera tienen el *'ga'*.

Por otro lado, *Ibarzelaiak* son *"los campos de la vega"*, pero están ligeramente más altos que la fuente de Azkarre,
por lo que es improbable que los regara el mismo arroyo que va a Marusas por el Hospital.
En *Iturrialde* es donde las aguas parecen ir en todas direcciones, porque es el límite entre las cuencas del Gobelas
y el Butrón.

Con todo ello, aunque la persona más longeva con la que había hablado era precisamente de *Iturri-beheko* y me afirmó
que no hay ninguna fuente más que Azkarre, estaba yo aún mosqueado con la trayectoria de la cantera de la Hípica a
*Iturriaga*.
Lo único que se me ocurría es que las aguas en *Iturriagaburu* encontraran una barrera de roca menos permeable y las
enviara hacia el Oeste, encontrándose con *Azkarburu* antes de aflorar en la fuente de Azkarre.

Para salir de dudas, el 7 de septiembre por la tardé fui de nuevo a *Iturriaga*.
Por la mañana sólo había encontrado a un joven, demasiado joven, para darme detalles, aunque me atendió muy amable y me
pudo aclarar lo que sí sabía.
Por la tarde, me encontré con su aita cortando el césped, y con su vecino Ugarte (el carpintero) paseando por el terreno
con el perro.
Ambos de atendieron con agrado y me explicaron lo que me faltaba por saber.

El vecino de la parte inferior me explicó que no estaba seguro del origen del nombre *Iturriaga*, pero que sí era cierto
que particularmente en la parte inferior de su terreno sale mucha más agua cuando llueve que en la parte superior.
También me indicó que en la parte media-inferior de la cuesta/carretera que da acceso a su casa hay una canalización de
sureste a noroeste, que precisamente lleva las aguas a reunirse con la canalización de Marusas.
Es decir, hay una canalización en *Ibarzelaiak* perpendicular a la carretera.
Al hablar con Ugarte, me confirmó que la toponimia no miente.
Particularmente en el extremo sureste de su parcela, la que ha quedado aislada por la curva de la calle Goieta, había un
manantial/fuente, pero *"lo taparon hace años porque era peligroso"*.
A ese manantial/fuente es a donde desagua la charca de la cantera noroeste Lukiatxa.
Además, por todo lo que es Iturriagaburu (donde la calle Goieta atraviesa el terreno) *"salía agua por todas partes"*.
Por lo tanto, de la suposición de trazados hecha observando las curvas de nivel, ¡el brazo más oriental existió!
Nos queda por averiguar si ese manantial/fuente y el arroyo tuvieron algún nombre.
Ugarte también me habló de la cueva de Santamariñealde.

Falta también hablar con los dos vecinos de la calle Goieta, entre Azkarre y Iturriaga, para ver si hay algún
regato o surgencia en el terreno de Gorka (el que tiene un pico entre las dos casas), o de ahí hacia abajo.
Con eso podríamos confirmar si existieron tres brazos del arroyo en Urduliz o sólo dos.

Es reseñable que la fuente de Azkarre, el manantial/surgencia de Iturriaga e Iturriagaburu están a una altura similar,
varios metros por debajo del cordal.
Es posible que haya varias placas semihorizontales con particular permeabilidad con respecto a las superiores, de forma
que el agua se filtra hasta esa profundidad pero después se ve proyectada hacia fuera.
Una de ellas concretamente a la altura de la fuente de Azkarre, que está a la misma altura que la parte baja del terreno
de Iturriaga.

## Contaminación del Gobela(s)

Estas son algunas fotografías tomadas el 4 de septiembre de 2023 en las parcelas 8001, 6001 y 6002 de Sopela.
Suponiendo que los propietarios sean Metro Bilbao, el Ayuntamiento de Sopela u otra administración pública, sería muy
recomendable ponerse en contacto con el Ayuntamiento de Sopela para hacerles saber sobre el estado.

::::{card-carousel} 3

:::{card}
![](../_static/img/umarcor/contaminaciongobela1.jpg)
:::

:::{card}
![](../_static/img/umarcor/contaminaciongobela2.jpg)
:::

:::{card}
![](../_static/img/umarcor/contaminaciongobela3.jpg)
:::

:::{card}
![](../_static/img/umarcor/contaminaciongobela4.jpg)
:::

:::{card}
![](../_static/img/umarcor/contaminaciongobela5.jpg)
:::

::::
