# Eskalada Eguna(k)

Hay muchas instituciones/empresas que organizan cursos y actividades en las peñas.
Además de cursos de formación como los de la Federación, para policías o bomberos, y actividades de aventura (por
ejemplo, [ABENTURA EGUNA PEÑAS DE SANTAMARINA (URDULIZ)](http://www.kirik.com/servicios/aventura-naturaleza/abentura-eguna-pe%C3%B1as-de-santamarina-%28urduliz%29/)), se han hecho actividades más llamativas:

- Escalada "nocturna" con los hermanos Pou organizado por Red Bull en Abril 2010
(ver [youtube.com/watch?v=iSA9ir68nl4](https://www.youtube.com/watch?v=iSA9ir68nl4)).
- Grabación de un videoclip del tema 'El Conquistador' con El Mentón de Fogarty y miembros del reality show.
  - [Conquistador del fin del mundo, Mentón de Fogarty](https://vimeo.com/67118094)
  - [Making of del videoclip de la canción del Conquis](https://www.eitb.eus/es/videos/detalle/1316612/video-making-of-videoclip-cancion-conquis/)

Sería interesante que una vez al año el Ayuntamiento hiciera una jornada o un fin de semana para que todos los usuarios
tanto particulares como profesionales pudieran encontrarse y aprovechar para ir comunicando las mejoras, los peligros, etc.

El "Eskalada Eguna" podría servir para concienciar sobre la importancia de hacer algun curso, e incluso el Ayuntamiento
o la Diputación podrían contratar guías titulados para ofrecer formación como una de las actividades a realizar ese día.

## 9 de septiembre de 2023

El 9 de septiembre de 2023 hicimos una quedada, usando las fiestas de Urduliz como excusa.
Por la mañana, dada la disposición actual del Ayuntamiento de Urduliz, hubo un primer contacto entre la comunidad de
escaladoras y la alcaldesa, de cara a conocer y compartir ciertas actuaciones (limpieza de la maleza, señalización de
los sectores, actualización de los croquis...).
El resto del día estuvimos escalando.
A media tarde, algunos fuimos a Lukiatxa y Kurtze a ver posibles bloques.
A última hora tomamos unos tragos en Txozna.

[Eskalada Eguna 20230909 Cartel](https://gitlab.com/urduliz/santa-marina/-/blob/main/EskaladaEguna_20230909_Cartel.pdf)

Valoramos muy positivamente la jornada, y esperamos poder repetirla el año que viene.
La coincidencia con las fiestas, aunque incómoda para los escaladores del pueblo porque tienen activides con las
cuadrillas, resulta atractivo para los de fuera que pueden aprovechar para tomar algo o pasar la noche.

## Marzo-Abril de 2024

En Marzo-Abril, cuando ya no suele llover tanto, esperamos reunirnos para quitar hierbajos, musgos y líquenes, y
cepillar la arena de los cantos.

## Junio-Julio de 2024

Considerando que la quedada de septiembre es festiva y más para adultos, y que la quedada de marzo-abril es más para
trabajar en la limpieza que para escalar, sería interesante organizar una tercera quedada después de que finalicen las
clases en los colegios e institutos para hacer un jornada más familiar y de cara a todo el mundo (no sólo quienes ya
escalan).
