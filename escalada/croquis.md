(croquis)=
# Croquis

```{important}
En las secciones {ref}`sectores:equipados` y {ref}`sectores:sin-equipar` se recopilan todos los croquis que conocemos
para cada sector, independientemente de que estén desactualizados o sean incorrectos.
En esas secciones recopilamos toda la información disponible de cada sector (accesos, estado, fotografías, etc.).

Complementariamente, en esta sección se discuten los croquis en su conjunto, con la intención de elaborar una guía
actualizada y correcta que pueda distribuirse a través de la página del Ayuntamiento de Urduliz, ponerse en carteles
junto al Parking o en el acceso a cada sector, etc.
```

## Contenido

### 1970-1980

```{figure} ../_static/img/InakiMiro_70s.jpg

Foto de la entrada [LA MONTAÑA Y LA ESCALADA, ESA OBSESIÓN... ](http://inakimiro.blogspot.com/2011/10/la-escalada-esa-obsesion.html)
del blog de Iñaki Miró, donde se ve la práctica de la escalada en Santa Marina N en los 70.
```

No tenemos del todo claro quién puso los primeros clavos, pero sabemos que desde mediados de los 70 había gente como
Miguelito,
[Iñaki Miró](https://luismigueleguiluz.blogspot.com/2019/04/penas-de-santa-marina.html?showComment=1568300056392#c7826811811581278484)
o Peke
trepando por las peñas.
Hoy cada uno tiene una vía dedicada (equipadas y nombradas durante la extensión de la Escuela coordinada por LME en
2017-2019):

- [Homenaje a Miguelito](https://www.thecrag.com/en/climbing/spain/urduliz/route/5393827965) en el sector Hípica.
- [Miró y CIA](https://www.thecrag.com/en/climbing/spain/urduliz/route/5395942782) en el sector Moruatxa Arista Sur.
- [Las txapas de 'Peke'](https://www.thecrag.com/en/climbing/spain/urduliz/route/5419596759) en el sector Moruatxa Arista Sur.

```{note}
Algunos como Iñaki Miró, ya no viven en Euskadi; sin embargo, Sherpa tiene o puede conseguir el contacto de todos ellos,
y está en ello.
Ha invitado a Miguelito a la quedada del día 9 de septiembre; y en cualquier caso seguiremos intentando reconstruir este
periodo de la historia de la Escuela.
Aunque no sea particularmente relevante para la elaboración de los croquis de las vías de escalada deportiva que hay
actualmente, es de recibo recordar a quienes nos precedieron.
```

Me han hablado también de Guillermo Bergaretxe, un escalador ya veterano de Urduliz, que solía escalar sin cuerda por el
norte del cordal.
Suele andar por Urduliz.

### 1980-2000

La práctica de la escalada deportiva en las Peñas de Santa Marina se remonta a los años ochenta.
Antes no se realizaba escalada deportiva como tal, sino clásica o artificial, como entrenamiento/preparación para
paredes más exigentes en Pirineos, Picos de Europa, los Alpes, etc.
En la década de los ochenta, en cierta manera la comunidad se dió cuenta de que podía equipar las vías desde arriba,
lo cual supuso el surgimiento de un nuevo deporte.
Con la evolución de la escalada deportiva, las vías se fueron reequipando y Urduliz como tantas otras Escuelas sufrió
una transformación.

Sherpa, junto con algunos de los ya mencionados y otros, participó esos años en la exploración/equipación de la pared
principal de Santa Marina y su reconversión para la práctica de la escalada deportiva, ya que entonces Moruatxa sólo se
usaba para prácticar rápeles.
Ya en los noventa, elaboró un par de guías en formato "fanzine" (la primera fechada en 1994, la segunda por concretar),
y contó con unos pocos sponsors para imprimir algunas copias.

En la primera de ellas, la descripción decía:

> Peñas de Santa Marina
>
> Urduliz - Escuela del veranillo
>
> Urduliz ha pasado de ser la escuela de las viejas clavijas y los buriles roñosos, a la escuela con un equipamiento a
> la última: anclajes químicos.
>
> A ello ha contribuido especialmente "Miguelito" y el que os lo cuenta, tanto en la labor de reequipamiento como en la
> apertura de nuevos itinerarios.
>
> La roca es arenisca de buena calidad, aunque suelen desprenderse gran número de granos de arena.
>
> Suele ser una escuela muy concurrida en verano debido a su orientación Norte, a su acceso (2 min del coche), a su base,
> y a la cercanía a las playas.
>
> Con el fin de aprovechar la zona a tope, se han tallado y pegado piedras, con lo cual, contamos con más de 40 vías.

Sherpa ha tenido la amabilidad de escanear todas las páginas de guía (que era sobre las Escuelas de Liendo y Urduliz).
Podéis verlas todas ellas en [pdf/sherpa/guia94](https://gitlab.com/urduliz/santa-marina/-/tree/main/pdf/sherpa/guia94).

La segunda guía tenía portada y contraportada a color, que es llamativo para un zine/croquis de la época.
No eran simples hojas fotocopiadas.

```{note}
Durante algunos años, al buscar información sobre los croquis de Urduliz, la respuesta era *"pregunta en la Txozna, que
ahí tienen copias"*.
La Txozna es un establecimiento hostelero situado junto a la Iglesia de Urduliz y fue durante muchos años el "comercio"
más cercado a la Escuela de Escalda.
Fue además uno de lo sponsors de aquellas primeras guías de Sherpa, y es conocido por haber organizado otro tipo de
actividades deportivas, como el campeonato de fútbol para cuadrillas que solía jugarse en el césped de su parte trasera.
```

```{important}
Cuando pueda, Sherpa me dará una copia de la segunda guía.
```

De cara a reconstruir el origen de los nombres de las vías en la pared principal de Santa Marina, Sherpa conoce el
motivo o inspiración de la mayoría de ellas, porque puso los nombres él mismo al elaborar las guías y/o porque estuvo
por ahí cuando se equiparon.
Con respecto a otros sectores, no tiene tan claro el origen y nombre de las vías de Escaleras o el Tejado, pero puede
preguntar.
Poco a poco iremos transcribiéndolo.

Para entender la situación entre los 80 y 90, es interesante el siguiente artículo del número dos de la revista
Santamariñe (que después pasaría a ser UK, y después Hiruka).
Como Koldo Somocueto, quién me ha facilitado el artículo, comenta: es muy llamativo que en el segundo número dedicaran
un artículo de esta extensión a la escalada; sin duda reflejo de la importancia que tenía.
A lo que añado que es reseñable que en el artículo se dice que hay que hacer las vías a vista, porque todavía no había
ninguna guía.
Puesto que el artículo se publicó en 1992, Miguelito y Sherpa ya habían reequipado la pared, pero todavía no se había
publicado/hecho la primera guía, la del 94.

```{note}
Estamos intentando localizar/contactar con el Asier que se menciona en el artículo.
```

::::{card-carousel} 2

:::{card}
![](../_static/img/SantaMarine2_Eskalada1.jpeg)
:::

:::{card}
![](../_static/img/SantaMarine2_Eskalada2.jpeg)
:::

:::{card}
![](../_static/img/SantaMarine2_Eskalada3.jpeg)
:::

::::

En el número 177 de Pyrenaica, publicado en el 94, se incluyó un artículo de 4 páginas sobre la escuela de escalada de
Urduliz, titulado [La Escuela de Santa Marina de Urduliz](https://pyrenaica.com/wp-content/uploads/hemeroteka/177/pdf/pyrenaica_177_48.pdf), firmado por Iban Ureta.
Aunque el croquis pretendía ser más detallado que las guías de Sherpa, desde la perspectiva actual resulta difícil
reconocer algunos de los trazados indicados.
Puede que de ahí provenga la diferencia de graduación en algunas de ellas.

La dificultad en escalada deportiva evolucionó rápidamente (en 1991 se encadenó el primer 9a de la historia, Action Directe en Frankenjura) y la atención se dirigió a otras Escuelas con
trazos más exigentes (en Urduliz, la graduación más común está entre 6a y 7a, y esta última decada se han equipado
muchas vías de III, IV y V).
Por ello, desde finales de los noventa y durante los 2000 no hubo actividad notable en cuanto a apertura/equipación de
nuevas vías.
De hecho, tampoco hubo mucho mantenimiento hasta 2016.


En [geocities.ws/monodedo_dinamico](http://www.geocities.ws/monodedo_dinamico/) encontramos una interesante cápsula
temporal.
Una página web que por la estética parece ser de finales de los noventa o muy primeros de los 2000.
Tiene una sección sobre la Escuela de Urduliz ([geocities.ws/monodedo_dinamico/urduliz.html](http://www.geocities.ws/monodedo_dinamico/urduliz.html)) donde se comenta: *"croquis en el bar al lado de la iglesia. Pronto subiremos fotos con
las vias marcadas"*.
A diferencia de las guías de Sherpa, estos croquis están hechos digitalmente (probablemente con Paint o algún programa
similar), pero la calidad es muy baja.
Podría ser precisamente un primer intento por hacer una versión digital de las guías de Sherpa.

La página en sí se presenta como un grupo de amigos que escalan.
No hay nombres completos, pero los nombres de las fotos en la sección [geocities.ws/monodedo_dinamico/fotos.html](http://www.geocities.ws/monodedo_dinamico/fotos.html)
incluyen los siguientes: Dani, Aitor, Joseba y Barri (perro).
El correo de contacto que figura en la página es `gbbnacad@lg.ehu.es`, que de acuerdo con [Memoria 2002-2055 de la SEBBM](https://sebbm.es/wp-content/uploads/Memoria-SEBBM-2002_2005.pdf)
corresponde a Alicia Blanco Santamaría, del Dpto. de Bioquímica de la Facultad de Ciencias de la UPV/EHU.
Sin embargo, parece no haber otros resultados en motores de búsqueda, tampoco en el buscador/directorio de la UPV/EHU, y
el teléfono de contacto que figura en la memoria corresponde con la centralita de la universidad.
Sería interesante contactar con Alicia para saber si puede/quiere contarnos algo del origen de esa web.

### 2000-2010

Desde que llegara la tarifa plana a principios de los 2000 y empezara a popularizarse internet en el ámbito doméstico,
la referencia más conocida de la escuela de escalada es la publicación Euskalarria de 2005, donde se documentaron las
paredes Santa Marina N, Santa Marina Escaleras y Santa Marina S.
Alguien escaneó las dos páginas que contenían los croquis de Urduliz, y esas imágenes han estado circulando por la red
durante 20 años.
Resulta sin duda llamativo que 1) nadie escaneara antes las guías de Sherpa o que no hayan sobrevivido en la red y 2)
que todavía en 2023 la gente mire en su móvil esas mismas dos páginas de la edición de 2005 de Euskalarria.
Es decir, muchos usuarios desconocen los croquis actualizados de LME, los de la página de la Diputación o las guías de
Sherpa.

En 2017 se publicó una versión actualizada, pero no he podido acceder a una copia para revisar el contenido.
A diferencia de la versión de 2005, parece que no han escaneado las páginas.
De hecho, no estoy seguro de que la edición de 2017 incluya la Escuela de Urduliz-Sopela; es posible que no esté.

Desconozco a quién/quienes consultaron los autores de Euskalarria 2005, si se basaron en la guía de Sherpa y cía, u
otros croquis como los de la página de monodedo_dinamico, o un poco de todo.

La última semana de Agosto, escribí un un e-mail muy escueto al contacto de [ekaitzmaiz.com](http://www.ekaitzmaiz.com/) y me ha respondió muy rápido indicándome que es uno de los autores de Euskalarria, pero él no conoce Urduliz.
Es Iñaki Marko (Kongi) quien elaboró la parte de Urduliz.
Todavía no he contactado con Kongi.

Además de preguntar cuáles fueron sus fuentes para elaborar Euskalarria en 2005 y 2017, sería interesante consultar
cómo realizan los croquis y valorar que el Ayuntamiento les encargue la elaboración de croquis actualizados (una vez que
ordenemos el caos de referencias actual).
No obstante, no tiene sentido adelantar acontecimientos.

### 2010-2020

En cualquier caso, seguramente Euskalarria 2.0 no contenga información actualizada, ya que se publicó en 2017.
Aunque no hubo prácticamente actividad hasta 2017, en 2018 y 2019 se abrieron varios sectores en el esfuerzo coordinado
por Luis Miguel Eguiluz.
Moruatxa prácticamente no se había reequipado para la práctica de escalada deportiva durante los 80 y 90.
Había algo de material, pero en 2018 y 2019 se extendió la Escuela en lo que respecta a las instalaciones de deportiva,
dando nombre a los sectores Moruatxa (Norte y Sur), Arista Sur, Pino (Bajo y Alto) e Hípica.

Ese trabajo se publicó principalmente en el blog de LME: [luismigueleguiluz.blogspot.com: Peñas de Urduliz/Sopela](https://luismigueleguiluz.blogspot.com/p/penas-de-urduliz.html).

```{attention}
Es importante señalar que LME no repitió el contenido de Euskalarria que no fue modificado sustancialmente.
Documentó los sectores nuevos y las vías añadidas a los sectores ya conocidos.
Finalmente, actualizó el croquis de la pared principal por completo, aunque su (re)interpretación de la misma requiere
algún ajuste.
```

En el número 271 de Pyrenaica, publicado en el 18, en un artículo titulado [Bizkaia 7tik 9ra](https://pyrenaica.com/wp-content/uploads/hemeroteka/271/pdf/pyrenaica_271_52.pdf), firmado por Alexander Arrizabalaga y Estibaliz Kerexeta, se mencionaba
y mostraba el croquis de una vía graduada como 7b en Urduliz denominada Urkatt.
Se agradecía su equipación Javier Antillera.
Resulta llamativo que se mencionara esa vía en particular, porque existen Potxolo 7b y Zumo de Chucrut 7c en la misma pared,
ambas muy conocidas.
Puede que el artículo consistiera en recorrer las escuelas mencionando vías equipadas por ellos.
En cualquier caso, no estoy seguro de que se conozca esa vía/trazado más allá de ese artículo concreto.
Sería interesante incluirla en los croquis actualizados que se realicen.

### 2020-Presente

En 2022, la Diputación publicó nuevos croquis en su página web:

- [bizkaia.eus/kirolak/escalada](https://www.bizkaia.eus/es/kirolak/escalada)
  - [URDULIZ](https://www.bizkaia.eus/es/kirolak/escalada/zona-escalada/-/asset_publisher/Jr1pkxIhZPnM/content/zona-urduliz/880303#com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_Jr1pkxIhZPnMbipo_contenedor_pestanias1)

Sin embargo, parece que reunieron la información por su cuenta, sin consultar con los equipadores.
Mientras que Sherpa y LME conocen algunos de los equipadores/aperturistas (entiendo que muchos de ellos son contactos en
común y otros no), parece que la edición de la Diputación está realizada en base a fotografías, croquis existentes y
puede que visitando las paredes.
En algunos sectores, se percibe una reedición de los croquis de LME, no exenta de ciertas reinterpretaciones de los trazos.
Otros, como Santa Marina Escaleras, parecen tener más de invención que de documentación.
Hay sectores conocidos que faltan, como el Tejado de la Ermita.

Actualmente, Sherpa esta rehaciendo el croquis de la pared principal de Santa Marina, ya que es sobre la que está
trabajando para optimizarla.
Está recolocando algunos químicos/chapas sólo para mejorar su ubicación, pero sin alterar las vías (en todo caso para
llevarlas más cerca del trazo original/habitual);
y por otra parte está abriendo algunas vías en la parte central e izquierda, donde no hay tanta congestión como en la
parte derecha y todavía hay sitio para algunas (pocas) vías nuevas.
El croquis actualizado que tiene lo está utilizando como borrador para preparar/arreglar esas vías.
Estos últimos 2-3 años hay otros equipadores como Javi que también han abierto alguna vía nueva en la pared principal.
Entiendo que Sherpa está incluyendo los cambios/ajustes que otros equipadores están haciendo en esa misma pared.
Por lo que entendí, sacaron una foto con un dron, pero no sacaron fotos de todos los sectores.

Deberíamos consensuar unos croquis actualizados que reflejen la tradición de escalada y la intención en su equipación,
ya que los reequipadores y mantenedores se guían por esos trazos.
Parece ser que entre Sherpa y LME pueden tener el conocimiento y los contactos para completar la información de todos
los sectores (entre ambos han nombrado prácticamente todas las vías existentes actualmente).
No obstante, sería recomendable publicitar un borrador y tratar de hacerselo llegar al mayor número de gente posible
para que en un plazo de uno o varios meses puedan hacer correcciones, modificaciones, sugerencias, etc.

Sería interesante disponer de la siguiente información por cada vía:

- Nombre.
- Sector.
- Tipo (Deportiva, Bloque, Varios largos).
- Dificultad.
- Número de químicos/chapas/cintas necesarios.
- Tipo de reunión (nº de puntos -típicamente 2 o 3- y si están unidos mediante cadena).
- Fotografía perpendicular, donde se indique la ubicación de las chapas, además del trazado.
- Longitud mínima de cuerda.
- Si dispone de acceso alternativo para top-rope.
- Origen (aperturista/equipador original, y razón del nombre).
- Última equipación/revisión.
- Incidencias (medio ambiente, estado del material, etc.)

### Búlder, Boulder, Bloque

Hay varios bloques y travesías en el cordal que permiten la práctica de la escalada en bloque/búlder/boulder.
Sin embargo, creo que nunca ha existido una guía de escala en bloque como tal.
Sería interesante que quienes practican esta modalidad de escalada elaboraran un guía señalando la ubicación de los
problemas y su dificultad.
De hecho, la ubicación de casi todos los bloques/secciones donde puede practicarse esta modalidad ya está identificada,
pero necesitamos alguien con experiencia suficiente en la disciplina como para graduarlos con un mínimo de criterio.

## Tecnología

En cuanto a la técnica, la solución tradicional de utilizar Paint/Gimp/Photoshop para dibujar sobre fotografías parece
insuficiente hoy en día.
Hay soluciones interactivas que permiten exponer con mayor claridad el nombre y trazado de cada vía.

En este sentido, la visulización de la página de la Diputación demuestra cierto interés, pero resulta insuficiente.
Además del mapa de los sectores (el mismo que hay en el cartel junto al parking), en la web están los croquis donde se
indica la longitud y dificultad de las vías.
Sin embargo, no se indica el número de chapas/químicos o su ubicación.
La web es interactiva en el ordenador, pero no es cómoda para imprimir o utilizar en el móvil.

Hay páginas específicas que utiliza la comunidad de escaladores, como puede ser
[thecrag.com/es/escalar/spain/urduliz](https://www.thecrag.com/es/escalar/spain/urduliz),
que permiten especificar el número de chapas/reuniones y su ubicación en la vía.

```{note}
Hay un usuario en thecrag.com (Aitor @mendizale), que lleva varios meses actualizando la información de varias escuelas
de escalada de euskadi, y particularmente las vías de Urduliz-Sopela.
Me he puesto en contacto con él a través de la plataforma para invitarle a acurdir a la quedada el 9 de septiembre, y
para ver si algún día podemos coincidir (con intención de preguntarle cuáles son sus fuentes de información, si es que
tiene alguna además de Euskalarria, el blog de Luismi o la página de la Diputación).
```

Deberíamos valorar si es pragmático acordar el uso de una página concreta, para que podamos actualizar y corregir los
croquis de forma colectiva; y después usar eso como referencia para hacer una versión impresa (léase en PDF) y/o para
poner en carteles.

## Marcado

Es habitual en escuelas de escalada (y en la montaña en general) marcar/indicar puntos importantes mediante piedras
apiladas (hitos, mojones), pintura o placas/buzones.
En las escuelas de escalada suelen indicarse los accesos a los sectores y los puntos de inicio de las vías.

### Accesos

Para indicar los accesos a los sectores se utilizan los mismos métodos que para otros senderos/rutas: hitos, postes/letreros o marcas de pintura.

En el caso de la escuela de Urduliz-Sopela, puesto que no hay una ruta que pase por ahí, lo más razonable resulta poner postes/letros a los largo del Paseo y Parque de la Memoria (similares a los que indican los nidos, o incluso utilizando el mismo poste).

### Vías

Para indicar el punto de inicio de las vías, suelen usarse tres soluciones:

- Poner una piedra de dimensiones notables o de forma llamativa en el suelo.
  Es la solución menos invasiva, pero tiene el inconveniente de que los usuarios pueden moverlas sin darse cuenta de su propósito.
  Se pueden pintar para hacerlas más evidentes, pero aún así no están fijas.
- Escribir el nombre en la pared (a 0.5-1 m de la base) usando un rotulador o pintura resistente.
  El inconveniente principal es la durabilidad y legibilidad.
  Por un lado, resulta difícil trazar letras sobre la arenisca sin hacerlas excesivamente grandes.
  Por otro lado, se desgastan/borran fácilmente.
- Pegar un canto rodado (a 0.5-1m de la base) con sika/cemento y pintarlo con pintura plástica densa.
  El inconviente principal es la tentación que pueda haber en ciertas vías de usar el canto como pie, cuando es un
  elemento que no pertenece a la vía.
  Con respecto a la legibilidad, al ser un canto con una superficie lisa, y al poder homogenizarla con una capa base, se
  pueden obtener mejores resultados que al escribir sobre la pared directamente.

Actualmente, se están utilizando dos soluciones:

- En Santa Marina N están escritos los nombres con rotulador.
- En Moruatxa S hay algunos cantos sikados donde se indican el nombre y la dificultad.

Creo que la solución más adecuada para la peñas de Urduliz-Sopela es fabricar placas mediante estampación o grabado
(como las chapas para mascotas o los buzones).
Pueden fijarse a la pared con uno o dos tornillos/clavos (utilizando sika como cuando se ponen cantos rodados), no
tienen el inconveniente de ser usados como pie, y no es necesario hacer 100 "a mano".
Si en unos años hay que reponer alguna o crear otras porque se han abierto vías nuevas, debería ser fácil mantener el diseño.

```{note}
Una placa de buzón grabada en aluminio parece estar sobre 4€.
Por lo tanto, el presupuesto aproximado para las más de 100 vías existentes es de 500€.
Sin embargo, debido a la oxidación, puede ser recomendable utilizar otro material.
```

```{note}
Como la dificultad de las vías es subjetiva y el grado de muchas no está asentado, creo que es suficiente con que las
placas indiquen el nombre de la vía, no la dificultad.
Opcionalmente, podemos imprimir QRs en plástico que unamos a las placas con bridas para que los usuarios puedan acceder
a toda la información de la misma (dificultad, croquis, número de chapas, tipos de reunión, prohibiciones, etc.).
```

```{note}
Con respecto a la nidificación, se podría poner un indicador de plástico con una brida, o indicarlo a en la página a la
que dirija el QR.
La brida puede ponerse en la chapa con el nombre de la vía, o en la primera chapa/químico.
De hecho, poner una cinta en la primera chapa/químico es una solución habitual para indicar que no se debe escalar una
vía (sea por fauna, flora, por el estado del material o por otras causas).

![](../_static/img/Bridas-de-Seguridad.jpg)
```
