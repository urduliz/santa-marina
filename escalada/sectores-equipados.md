(sectores:equipados)=
# Sectores equipados

```{attention}
Las imágenes y croquis en esta sección pueden no representar el estado actual de los sectores.
Es un recopilación de los últimos 6 años, por lo que ha habido cambios, tanto de vegetacion como desprendimientos.
Ver discusión sobre la actualización de los croquis en la sección {ref}`croquis`.
```

## Santa Marina

### N | Principal

::::{card-carousel} 4

:::{card}
![](../_static/img/monodedo_dinamico/SantaMarinaN.png)

Croquis Santa Marina N, monodedo_dinamico.
:::

:::{card}
![](../_static/img/monodedo_dinamico/SantaMarinaNO.png)

Croquis Santa Marina NO, monodedo_dinamico.
:::

:::{card}
![](../_static/img/euskalarria/croquispeassantamarinaurdulizp.jpg)

Croquis Santa Marina N, Euskalarria, 2005.
:::

:::{card}
![](../_static/img/luismigueleguiluz/CSantaMarinaN.jpg)

Croquis Santa Marina N, Luis Miguel Eguiluz, 2019.
:::

:::{card}
![](../_static/img/luismigueleguiluz/CSantaMarinaNArenasMovedizasEnergiaCero.jpg)

Croquis Santa Marina NE, Luis Miguel Eguiluz, 2019.
:::

:::{card}
![](../_static/img/dfb/SantaMarinaNE.jpg)

Croquis Santa Marina NE, Diputación Foral de Bizkaia, 2022.
:::

:::{card}
![](../_static/img/dfb/SantaMarinaN.jpg)

Croquis Santa Marina N, Diputación Foral de Bizkaia, 2022.
:::

:::{card}
![](../_static/img/dfb/SantaMarinaNO.jpg)

Croquis Santa Marina N, Diputación Foral de Bizkaia, 2022.
:::

::::

::::{card-carousel} 2

:::{card}
![](../_static/img/luismigueleguiluz/SantaMarinaN1.jpg)

Foto de Santa Marina N, [Peñas de Santa Marina, una escuela viva](https://luismigueleguiluz.blogspot.com/2020/05/penas-de-santa-marina-una-escuela-viva.html), 2020.
:::

:::{card}
![](../_static/img/luismigueleguiluz/SantaMarinaN2.jpg)

Foto de Santa Marina NE, [Peñas de Santa Marina, una escuela viva](https://luismigueleguiluz.blogspot.com/2020/05/penas-de-santa-marina-una-escuela-viva.html), 2020.
:::

::::

### S | Ikurriña

::::{card-carousel} 2

:::{card}
![](../_static/img/euskalarria/croquispeassantamarinaurduliza.jpg)

Croquis de Santa Marina S, Euskalarria, 2005.
:::

:::{card}
![](../_static/img/dfb/SantaMarinaS.jpg)

Croquis de Santa Marina S, Diputación Foral de Bizkaia, 2022.
:::

:::{card}
![](../_static/img/luismigueleguiluz/CSantaMarinaS.jpg)

Croquis de Santa Marina S, Luis Miguel Eguiluz, 2019.
:::

::::

Es reseñable que hay aproximadamente 15 reuniones "inútiles" en la base de este sector.
Hay más reuniones en la base de la pared que en la vías de este sector, lo cual es muy llamativo.
Esto se explica por la cantidad de instituciones y empresas que realizan actividades de formación en las peñas.
Sin embargo, es improbable que hayan sido los equipadores conocidos quienes hayan "desperdiciado" tal cantidad de material.
A falta de acceso a la memoria de reequipación de la Federación/Diputación del año 2022, queda la duda de si alguien más pudiera haber invertido ese material.

### O | Escaleras

::::{card-carousel} 3

:::{card}
![](../_static/img/monodedo_dinamico/Escaleras.png)

Croquis Santa Marina Escaleras, monodedo_dinamico.
:::

:::{card}
![](../_static/img/euskalarria/croquispeassantamarinaurduliza.jpg)

Croquis Santa Marina Escaleras, Euskalarria, 2005.
:::

:::{card}
![](../_static/img/dfb/SantaMarinaOErmita.jpg)

Croquis Santa Marina Ermita, Diputación Foral de Bizkaia, 2022.
:::

:::{card}
![](../_static/img/dfb/SantaMarinaOEscaleras.jpg)

Croquis Santa Marina Escaleras, Diputación Foral de Bizkaia, 2022.
:::

:::{card}
![](../_static/img/luismigueleguiluz/CEscalerasIntsusa.jpg)

Croquis Santa Marina Escaleras, Luis Miguel Eguiluz, 2019.
:::

:::{card}
![](../_static/img/luismigueleguiluz/CEscalerasDiedroArteaFisura.jpg)

Croquis Santa Marina Ermita, Luis Miguel Eguiluz, 2019.
:::

::::

#### Travesía y bloque

![](../_static/img/umarcor/SantaMarinaEscalerasBloque.jpeg)

### Tejado

::::{card-carousel} 1

:::{card}
![](../_static/img/umarcor/SantaMarinaTejado.jpeg)
:::

:::{card}
![](../_static/img/umarcor/SantaMarinaTejado2.jpeg)
:::

::::

En el tejado y la parte trasera de la ermita, junto al nido de ametralladora, hay un par de vías equipadas (no sé si han sido reequipadas) que dan a la Ikurriña.
Hay al menos una piedra pegada/sikada en una de ellas.

También hay una vía que empieza en el tejado y sube a la Ikurriña por el Oeste.
Esta no está reequipada.

En principio, no está permitido escalar desde el tejado.
Por el uso del tejado, se rompe el metacrilato de los tragaluces frecuentemente.
La ermita pertenece a la Iglesia, pero la Iglesia no paga.
En realidad, es improbable que sean escaladores quienes estropeen/rompan el tejado, ya que está lleno de pintadas y restos de "otras formas de ocio", y esa vía en particular no es muy conocida.
Sin embargo, es una prohibición razonable, dada la dificultad de acometer una actuación para instalar una pasarela que permitiera el uso deportivo.

## Moruatxa

### S

::::{card-carousel} 2

:::{card}
![](../_static/img/dfb/MoruatxaSO.jpg)
:::

:::{card}
![](../_static/img/dfb/MoruatxaS.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/CMoruatxaS.jpg)
:::

::::

::::{card-carousel} 2

:::{card}
![](../_static/img/luismigueleguiluz/MoruatxaS1.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/MoruatxaS2.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/MoruatxaS3.jpg)
:::

::::

### N Bajo

::::{card-carousel} 2

:::{card}
![](../_static/img/umarcor/MoruatxaBajo0.jpeg)
:::

:::{card}
![](../_static/img/umarcor/MoruatxaBajo1.jpeg)
:::

:::{card}
![](../_static/img/umarcor/MoruatxaBajo2.jpeg)
:::

::::

Del mismo modo que el sector Pino se divide en Pino Bajo y Pino Alto, el sector Moruatxa N también tiene una parte Baja, al nivel del paseo del Parque de la Memoria, y una parte Alta.
De hecho, es posible enlazar las vías de la parte Baja con la parte Alta en un largo de 35 m o en dos de 20-25 m + 10-15 m,
de forma similar a la vía Gatzarriñe en la cara S.
Sin embargo, Moruatxa N Bajo, no aparece en los croquis ya que la pared ha sido limpiada y equipada recientemente.
Además, puede que se encuentre en propiedad privada y por eso no se hubiera equipado antes.

### N Alto

::::{card-carousel} 2

:::{card}
![](../_static/img/dfb/MoruatxaNAlto.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/CMoruatxaNA.jpg)
:::

::::

![](../_static/img/luismigueleguiluz/MoruatxaNA.jpg)

Hay un cable de acero en la mitad más alta de la repisa, pero no en la baja.
Sería recomendable extender el cable hasta el principio (oeste de la repisa).

Asimismo, las primeras chapas de las vías Txirrista, Goroldio y Tximini gorria están un poco lejos para que el asegurador pueda usarlas.
Su ubicación es adecuada para la práctica de la escalada deportiva asumiendo que el asegurador está en suelo firme.
Sin embargo, puesto que Moruatxa N Bajo se encuentra inmediatamente debajo de la repisa, sería muy conveniente instalar una o dos reuniones (doble químico/chapa con cadena) en las bases de estas vías.
Esas reuniones podrían usarse por un lado para que los aseguradores se anclen a la pared, y por otro lado como reunión para enlazar vías de Moruatxa N Bajo con Moruatxa N Alto (en dos o tres largos).

### Travesía Arista

![](../_static/img/luismigueleguiluz/CTravesiaMoruatxaArista.jpg)

- Tracks wikiloc
  - [Arista Moruatxa Urduliz por Aintzane Martin Etayo](https://es.wikiloc.com/rutas-escalada/arista-moruatxa-urduliz-137531582)
  - [Arista Moro Haitza (Urduliz) por D@bid](https://es.wikiloc.com/rutas-escalada/arista-moro-haitza-urduliz-136285271)

Falta la reunión para pasar de Moruatxa N Alto hacia el este por el norte (que daría a parar a Pino Alto).
Hay tres varillas sikadas, pero faltan las chapas y cadenas.
Sin embargo, si que está la reunión para pasar del norte al sur.

## Moruatxa-Arista

### Pino Bajo

::::{card-carousel} 1

:::{card}
![](../_static/img/dfb/PinoBajo.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/CPinoBajo.jpg)
:::

::::

![](../_static/img/umarcor/PinoBajoDesdePino.jpeg)

### Pino Alto

::::{card-carousel} 1

:::{card}
![](../_static/img/dfb/PinoAlto.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/CPinoAlto.jpg)
:::

::::

::::{card-carousel} 2

:::{card}
![](../_static/img/luismigueleguiluz/PinoAlto1.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/PinoAlto2.jpg)
:::

:::{card}
![](../_static/img/umarcor/PinoDesdePinoAlto.jpeg)
:::

::::

Faltan dos chapas en el inicio de dos vías.
Están las varillas sicadas, pero faltan las chapas (y las tuercas).
Está indicado en thecrag.

Se puede acceder del oeste de la base de Pino Alto al alto de Pino Bajo para hacer instalaciones top-rope.

### S

::::{card-carousel} 2

:::{card}
![](../_static/img/dfb/MoruatxaAristaS.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/CMoruatxaAristaSO.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/CMoruatxaAristaSE.jpg)
:::

::::

![](../_static/img/ander/MoruatxaAristaSPaso.jpg)

## Hípica

::::{card-carousel} 1

:::{card}
![](../_static/img/dfb/Hipica.jpg)
:::

:::{card}
![](../_static/img/luismigueleguiluz/CHipica.jpg)
:::

::::

![](../_static/img/luismigueleguiluz/Hipica.jpg)
