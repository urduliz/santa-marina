(sectores:sin-equipar)=
# Sectores sin (re)equipar

De la veintena de sectores, bloques o travesías de escalada indicados en el mapa interactivo, sólo ocho sectores están actualmente documentados en la página de la Diputación.
A continuación se describen los sectores que aparecen en el {ref}`mapa interactivo <mapa>` pero no en otra documentación.

Los accesos a Santa Marina Tejado, Moruatxa N Bajo y Lukiatxa existen y son conocidos/evidentes (salvo cuando crece la vegetación por falta de mantenimiento).
Los primeros dos sólo hay que documentarlos (croquis) y el tercero se podrá equipar en cuanto haya interés.
Sin embargo, hay que aclarar la situación de los accesos a los posibles sectores Parking, Erdikoatxa y Atxarte.
Ver sección Parcelas.

## Torre Barri

![](../_static/img/ander/TorreBarriS.jpg)

En su cara S tiene varias paredes de igual o mayor altura que la cara S de Santa Marina.
Sin embargo, es propiedad particular, y tienen huerta hasta la base de la pared; por lo que no parece utilizable a corto
plazo.

Pendiente de revisar la cara N, aunque en principio no tiene interés, salvo igual para algo de bloque.

Ver información sobre parcelas {ref}`parcelas:0153` y {ref}`parcelas:0121`.
Ver información sobre el sendero por la cresta del cordal en {ref}`cordal`.

## Parking

Hay tres parcelas desde el Parking hasta la parcela de la curva de Torre Barri.
Dos de ellas tienen casas, una de las cuales creo que es para turismo rural.
No parecen utilizar el terreno más próximo a la pared norte de cordal.
La última (penúltima), más próxima a Torre Barri creo que puede pertenecer a la parcela central, la del cruce.

### N

::::{card-carousel} 2

:::{card}
![](../_static/img/umarcor/ParkingN.jpeg)
:::

:::{card}
![](../_static/img/umarcor/ParkingCresta.jpeg)
:::

:::{card}
![](../_static/img/umarcor/ParkingNchapa.jpeg)
:::

::::

En el Parking hay una cresta que permite hacer una travesía de bloque de unos 10 m en la base de la cara N.
Tiene una sola chapa en mal estado, ya que tiene 3-4 m de altura en la cara N pero 5-6 m en la cara S.

### S

![](../_static/img/umarcor/ParkingS.jpeg)

Hacia el sur pueden abrirse vías fáciles, similares a las de Santa Marina S.
Tiene unos 30 m de extension.

Sin embargo, es propiedad privada y un camino que según Google Maps debería atravesar esa parcela está desaparecido (ver
{ref}`Imprecisiones en Google Maps <viascomunicacion:imprecisiones:GoogleMaps>`).
Ver información sobre parcelas {ref}`parcelas:0127` y {ref}`parcelas:0430`.

### SO

::::{card-carousel} 1

:::{card}
![](../_static/img/umarcor/ParkingSO.jpeg)
:::

:::{card}
![](../_static/img/umarcor/ParkingSODesdeParkingS.jpeg)
:::

::::

Al final, a unos 5-10 m hacia el oeste, hay un bloque donde en la cara sur también pueden equiparse 4-5 vías similares a
las del sector Escaleras; o simplemente limpiar la base y hacer bloque.

### NO

La cara norte de este cordal tiene demasiada vegetación y arbolaje, pero parece tener mayor verticalidad y altura
ligeramente mayor que la cara sur (cosa que también sucede en Santa Marina, Muruatxa y Lukiatxa).

Sería interesante explorar la pared norte de la parcela más próxima al parking.
De hecho, el nacimiento del río Gobela podría encontrarse ahí.

(erdikoatxa)=
## Erdikoatxa

![](../_static/img/ander/Erdikoatxa.jpg)

No parece haber ninguna vía equipada.
Su cara S refleja la actividad minera pasada.
Podrían equiparse varias vías sencillas, similares a las de Santa Marina S, pero con un poco más de inclinación.
En el N parece que hay tanta altura y verticalidad como en Santa Marina N, pero hay mucha vegetación.

Tanto el N como S son propiedad particular.

### N

Es en cierta manera la continuación del sector Hípica.
Si se equipara esta pared, tendría sentido que el sector pasara a llamarse Erdikoatxa N.

El propietario de la parcela al norte (ver {ref}`parcelas:0038`) está muy interesado en que se mejore el trazado de los
itinerarios/senderos relacionados con los restos del Cinturón de Hierro.
Además de facilitar el acceso del Parque de la Memoria a la calle Goieta, querría que hubiera alguna instalación que
permitiera pasar Erdikoatxa por la cresta sin necesidad de equipación especial.
En este sentido, podría ser posible que estuviera dispuesto a llegar a algún acuerdo para que pueda practicarse escalada
en la cara N, si se plantea como un conjunto de habilitación de Erdikoatxa.
Particularmente porque la hípica que hay en ese terreno no llega hasta la base de la pared.
De hecho, ahí había un nido de ametralladora que se perdió (por el avance de la cantera después de la Guerra Civil).
En una (re)habilitación de esa pared para escalada y para senderistas podría incluirse alguna referencia a ese nido.

Nótese que una habilitación para senderistas no podría ser sólo una cadena o cable, de forma similar a los pasamanos de Muruatxa N Alto, Arista S, o el que debería ser repuesto en el alto del sector Hípica.
Una instalación así podría dar una falsa sensación de seguridad, ya que debería usarse obligatoriamente con un arnes un
cabo de anclaje doble (similar a las vías ferratas, siendo el disipador opcional en este caso), pero algunos senderistas
podrían estar tentados a hacerlo sin arnés.

### S

En principio el propietario de la parcela {ref}`parcelas:0281` no parece contento con que los senderistas recorran su
terreno.
Actualmente, como la cresta no está equipada y está un poco expuesta, los senderistas atraviesan el terreno por el sur,
a unos 20m de la cresta.
Desconozco si lo que le molesta es sólo que pasen, que puedan coger las setas que aparezcan, o qué razón puede haber.
En cualquier caso, hay vallas delimitando la linde, y hay restos de que haya habido algún cartel, aunque los senderistas
pasan igualmente.
Sin implicación del Ayuntamiento de Sopela, ya resulta complicado afrontar la comunicación con este propietario, como
para plantearnos hablar de la práctica de la escalada.

Creo que alguno de los equipadores ha estado echando un ojo para abrir alguna vía similar a las de la pared sur de Santa
Marina, aunque ya he hecho saber indirectamente que es propiedad privada.

## Lukiatxa

Hay restos de equipación en al menos dos puntos (en la placa al O del Mirador y en el bloque junto al nido de ametralladora más próximo al mismo).
Se trata de equipación muy vieja y en muy mal estado.

En ese mismo sector hay varias opciones para realizar escalada en bloque/búlder (sin cuerda).
En algún bloque se observan restos de magnesio relativamente recientes y se ve que han desenterrado parte de la base.

### N

La parcela {ref}`parcelas:0032` es de propiedad municipal.

La cara N es posible que tenga alguna equipación también, ya que hay varios perfiles muy sugerentes.
Se ve una reunión con una cinta en lo alto de uno de esos perfiles (subiendo por la pista directa del itinerario, en vez
de la zigzagueante).
La última semana de agosto o primera de septiembre de 2023 alguien ha instalado 4-5 variallas roscadas con sika, pero no
hay chapas.

![](../_static/img/umarcor/LukiatxaN.jpeg)

### Mirador

![](../_static/img/ander/LukiatxaMirador.jpg)

### Mirador Travesía

### Nido

Creo que está en una parcela que es propiedad particular, pero hay que confirmarlo con GPS.

::::{card-carousel} 2

:::{card}
![](../_static/img/umarcor/LukiatxaNido.jpeg)
:::

:::{card}
![](../_static/img/ander/LukiatxaNido.jpg)
:::

:::{card}
![](../_static/img/ander/LukiatxaNido2.jpg)
:::

::::

### Nido Bloque

![](../_static/img/umarcor/LukiatxaNidoBloque.jpeg)

Creo que está en una parcela que es propiedad particular, pero hay que confirmarlo con GPS.

Alguien está desenterrándolo.
He intentado hacerle saber indirectamente que está en propiedad particular.

(atxarte)=
## Atxarte

La antigua cantera en el extremo oriental del cordal, junto al nuevo cementerio de Urduliz, tiene una pared interesante
también.
Hacia el norte no tiene interés porque es tierra o piedra de mala calidad; hacia el sur tiene 30-50 m similares
a Santa Marina S, pero con mayor inclinación.

Se puede recorrer por la cresta casi en su totalidad, a excepción de 5-10 m en su parte más occidental.

![](../_static/img/ander/AtxartePasoExpuesto.jpg)

Podría valorarse equipar ese paso pero hay que pensarlo detenidamente.
En caso de no equiparse, es evidente por la exposición que no es seguro.
Si se equipara con un cable de acero, pensado para ser usados con arnés como si se tratara de una vía ferrata, se corre
el riesgo de que algunas personas traten de recorrerlo sin arnés, sujetándose únicamente con las manos (lo que es muy
peligroso).

::::{card-carousel} 2

:::{card}
![](../_static/img/ander/AtxarteN.jpg)
:::

:::{card}
![](../_static/img/ander/AtxarteNHito.jpg)
:::

:::{card}
![](../_static/img/ander/AtxarteNBajada.jpg)
:::

::::

Lo más sensato sería habilitar "escaleras" con tablones y varilla, para facilitar el descenso hacia el norte, hacia el interior de la cantera, de forma que se rodee el paso.

![](../_static/img/Catastro_Atxarte.jpeg)

![](../_static/img/SIGPAC_Atxarte.jpeg)

Mirando el Catastro y SIGPAC, parece que toda la pared sur está principalmente en la parcela 1.
Sólo un poco de la parcela 4.
En la 2 parece que está el bloque descompuesto (el que habrían de evitar los senderistas).
Por lo que en principio no deberíamos tener ningún problema con el vecino de Sopela de la parcela 164
(el que tenía/tiene vallado un tramo bien y otro con la valla caída).

### Atxarte S

::::{card-carousel} 2

:::{card}
![](../_static/img/umarcor/AtxarteS.jpeg)
:::

:::{card}
![](../_static/img/umarcor/AtxarteMojon.jpeg)
:::

:::{card}
![](../_static/img/ander/AtxarteS.jpg)
:::

::::

### Atxarte O

::::{card-carousel} 1

:::{card}
![](../_static/img/umarcor/AtxarteOBloque.jpeg)
:::

:::{card}
![](../_static/img/ander/AtxarteOBloque.jpg)
:::

::::
