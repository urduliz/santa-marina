/*
Authors:
	Aitor Oriñuela Salaverria
	Unai Martinez-Corral <unai.martinezcorral@ehu.eus>

Copyright Aitor Oriñuela Salaverria

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

SPDX-License-Identifier: Apache-2.0
*/

/*
	Base layers
*/

var osm = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '© OpenStreetMap'
});

/*
Hay 9 capas en https://geo.bizkaia.eus/arcgisserverinspire/services/Kartografia_Cartografia/WMS_Ortoargazkiak_BFA/MapServer/WMSServer?request=GetCapabilities&service=WMS
Parecen ser las mismas que se listan como ORTO_BFA_ en https://geo.bizkaia.eus/arcgisserverinspire/rest/services/Kartografia_Cartografia.
También se hace referencia a ORTO_EJ_1945, ORTO_EJ_1977 y ORTO_EJ_2022.

Por un lado, no parece del todo adecuado identificarlas por el número de capa, ya que si se añaden nuevas podría cambiar
el orden.
Sería recomendable poder identificar la capa en función del campo 'Title', en vez de 'Name'.

Por otro lado, en el visor de https://www.bizkaia.eus/es/cartografia-y-ortofotos, seleccionando Ortofotos del municipio
Urduliz hay resultados anuales desde 2004, y nueve resultados entre 1956 y 2004.
Es posible que los resultados entre 2004 y 2022 sean los mismos que los de geoEuskadi, y por eso no estén disponibles a
través del WMS de la Diputación y sólo ser sirvan los nueve resultados anteriores.
Sin embargo, geoEuskadi incluye también resultados de 2002, 2001, 1991, 1984-1985, 1977-1978, 1956-1957 y 1945-1946, y
las versiones de 2002 de geoEuskadi (ORTO_2002) y de la Diputación (ORTO_BFA_2002) no son las mismas.
Por lo tanto, habría que comprobar si ORTO_EJ_1945, ORTO_EJ_1977 y ORTO_EJ_2022 corresponden a ORTO_1945_46_AMERICANO,
ORTO_1956_57_AMERICANO y ORTO_2022, respectivamente.
*/

var ortofotos_diputacion = {};
for (const [key, value] of Object.entries({
	"Ortofoto - 2002 DFB": 0, //ORTO_BFA_2002
	"Ortofoto - 1999 DFB": 1, //ORTO_BFA_1999
	"Ortofoto - 1995 DFB": 2, //ORTO_BFA_1995
	"Ortofoto - 1990 DFB": 3, //ORTO_BFA_1990
	"Ortofoto - 1983 DFB": 4, //ORTO_BFA_1983
	"Ortofoto - 1975 DFB (parcial)": 5, //ORTO_BFA_1975
	"Ortofoto - 1970 DFB": 6, //ORTO_BFA_1970
	"Ortofoto - 1965 DFB": 7, //ORTO_BFA_1965
	"Ortofoto - 1956 DFB": 8, //ORTO_BFA_1956
})) {
	ortofotos_diputacion[key] = L.tileLayer.wms('https://geo.bizkaia.eus/arcgisserverinspire/services/Kartografia_Cartografia/WMS_Ortoargazkiak_BFA/MapServer/WMSServer?', {
		layers: value,
		attribution: '&copy <a href="https://www.bizkaia.eus/es/inspirebizkaia#wms">DFB</a>'
	});
}

var ortofotos_euskadi = {};
for (const [key, value] of Object.entries({
	"Ortofoto - Más actualizada geoEuskadi": "ORTO_EGUNERATUENA_MAS_ACTUALIZADA",
	//"Ortofoto - 2022 geoEuskadi": "ORTO_2022",
	"Ortofoto - 2021 geoEuskadi": "ORTO_2021",
	"Ortofoto - 2020 geoEuskadi": "ORTO_2020",
	"Ortofoto - 2019 geoEuskadi": "ORTO_2019",
	"Ortofoto - 2018 geoEuskadi": "ORTO_2018",
	"Ortofoto - 2017 geoEuskadi": "ORTO_2017",
	"Ortofoto - 2016 geoEuskadi": "ORTO_2016",
	"Ortofoto - 2015 geoEuskadi": "ORTO_2015",
	"Ortofoto - 2014 geoEuskadi": "ORTO_2014",
	"Ortofoto - 2013 geoEuskadi": "ORTO_2013",
	"Ortofoto - 2012 geoEuskadi": "ORTO_2012",
	"Ortofoto - 2011 geoEuskadi": "ORTO_2011",
	"Ortofoto - 2010 geoEuskadi": "ORTO_2010",
	"Ortofoto - 2009 geoEuskadi": "ORTO_2009",
	"Ortofoto - 2008 geoEuskadi": "ORTO_2008",
	"Ortofoto - 2007 geoEuskadi": "ORTO_2007",
	"Ortofoto - 2006 geoEuskadi": "ORTO_2006",
	"Ortofoto - 2005 geoEuskadi": "ORTO_2005",
	"Ortofoto - 2004 geoEuskadi": "ORTO_2004",
	"Ortofoto - 2002 geoEuskadi": "ORTO_2002",
	"Ortofoto - 2001 geoEuskadi": "ORTO_2001",
	"Ortofoto - 1991 geoEuskadi": "ORTO_1991",
	"Ortofoto - 1984-1985 geoEuskadi": "ORTO_1984_85",
	"Ortofoto - 1977-1978 geoEuskadi (interministerial)": "ORTO_INTERMINISTERIAL_1977_78",
	"Ortofoto - 1956-1957 geoEuskadi (americano)": "ORTO_1956_57_AMERICANO",
  "Ortofoto - 1945-1946 geoEuskadi (americano)": "ORTO_1945_46_AMERICANO",
})) {
	ortofotos_euskadi[key] = L.tileLayer.wms('http://www.geo.euskadi.eus/WMS_ORTOARGAZKIAK', {
		layers: value,
		attribution: '&copy <a href="https://www.geo.euskadi.eus/inicio">Geoeuskadi</a>'
	});
}

/*
	Overlays
*/

var municipios = L.geoJSON(MunicipiosColindantes, {style: {color: "#000", fill: false}});

var parcelas_urduliz = L.geoJSON(ParcelasUrduliz, {
	style: function(feature) {
		if ([
			'089000100030',
			'089000100031',
			'089000100032',
			'089000100060',
			'089000100060',
			'089000100061',
			'089000100079',
			'089000100081',
			'089000100082',
			'089000100251',
			'089100606001',
			'089100139001',
			'089100714001',
			'089100812001',
			'089101107001',
			'089101518001',
			'089101708004',
			'089101815001',
		].includes(feature.properties.Cod_Catas)) {
			return {color: "#0ff", weight: 1, fillOpacity: 0.25}
		}
		if ([
			'089000100148',
			'089000100153',
			'089000100154',
		].includes(feature.properties.Cod_Catas)) {
			return {color: "#f0f", weight: 1, fillOpacity: 0.25}
		}
		return {color: "#00f", weight: 1, fillOpacity: 0.25}
	},
	onEachFeature: function (feature, layer) {
		let props = feature.properties
		layer.bindPopup(`${props.Codigo_Mun} ${String(props.Codigo_Pol).padStart(4, '0')} ${String(props.Codigo_Par).padStart(4, '0')}`);
	}
});

var parcelas_sopela = L.geoJSON(ParcelasSopela, {style: {color: "#0000ff", weight: 1, fillOpacity: 0.1}});

/*
const rios = L.geoJSON(Rios_4326, {style: {color: '#0066CC'}});

const curvas = L.layerGroup([
	L.geoJSON(Curvas_nivel_4326, {style: {color: '#CFA386',weight: 2}}),
	L.geoJSON(Curvas_maestras_4326, {style: {color: '#A45726',weight: 4}})
]);
*/

const SectoresColores = {
  "Nagusia": '#66CC99',
  "Ermita-Escaleras": '#669966',
  "Ikurriña Sur": '#CCFF99',
  "Moruatxa Norte": '#99CC99',
  "Moruatxa Sur": '#9999CC',
  "Pino": '#CCCC99',
  "Arista": '#CC3333',
  "Hipica": '#0066CC',
}

function getColor(labelName) {
  return (labelName in SectoresColores) ? SectoresColores[labelName] : '#9999CC';
}

var OverlaySectores = L.geoJSON(
	SectoresEscalada,
	{
		style: feature => ({
				fillColor: getColor(feature.properties.labelName),
				weight: 2,
				opacity: 0.4,
				color: 'white',
				dashArray: '0',
				fillOpacity: 0.5
		}),
		onEachFeature: function (feature, layer) {
			if (feature.properties && feature.properties.labelName) {
				layer.bindPopup(feature.properties.labelName);
			}
		}
	}
);

/*
	Insertar imagenes
*/

/*
const customOptions = {
	'maxWidth': '500',
	'className' : 'custom'
}

const iconoImagenes = L.icon({
	iconUrl: 'icons/camara.png',
	iconSize: [20, 20], // size of the icon
});

const imagenes = L.layerGroup([
	L.marker([43.372294, -2.959753], {icon: iconoImagenes}).bindPopup(
		"Sector Pared Principal<br/><img src='images/01_ParedPrincipal_3_lvl.jpg' width='350px'/>",
		customOptions
	),
	L.marker([43.372232, -2.960225], {icon: iconoImagenes}).bindPopup(
		"Sector Escaleras<br/><img src='images/02_Eskalera_lvl.jpg' width='350px'/>",
		customOptions
	),
	L.marker([43.371497, -2.958694], {icon: iconoImagenes}).bindPopup(
		"Sector Moruatxa Norte<br/><img src='images/04_MoruatxaNorte_lvl.jpg' width='350px'/>",
		customOptions
	),
	L.marker([43.371023, -2.957417], {icon: iconoImagenes}).bindPopup(
		"Sector Hipica<br/><img src='images/08_Hipica_lvl.jpg' width='350px'/>",
		customOptions
	),
	L.marker([43.372816, -2.960436], {
		icon: L.icon({
			iconUrl: 'icons/parking.png',
			iconSize: [20, 20], // size of the icon
		})
	})
]);
*/

/*
	Create map and controls
*/

var map = L.map('map', {
	center: [43.372109, -2.959880],
	zoom: 16,
	layers: [osm]
});

L.control.layers(
	Object.assign(
		{
			"OpenStreetMap": osm,
		},
		ortofotos_euskadi,
		ortofotos_diputacion,
	),
	{
		"Municipios colindantes": municipios,
		"Catastro Urduliz": parcelas_urduliz,
		"Catastro Sopela": parcelas_sopela,
		//"Hidrografía": rios,
		//"Curvas de nivel": curvas,
		"Sectores Escalada": OverlaySectores,
		//"Imagenes": imagenes
	},
	/*
	{
		position: 'topright',
		collapsed: false
	}
	*/
).addTo(map);
